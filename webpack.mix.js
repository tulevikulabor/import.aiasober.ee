const mix = require('laravel-mix');
const fs = require('fs');
const fg = require('fast-glob');

const ImageminPlugin = require('imagemin-webpack-plugin').default;
const ImageminMozjpeg = require('imagemin-mozjpeg');
const CopyPlugin = require('copy-webpack-plugin');

const COUNTRIES = ['gb', 'ee', 'ru', 'fi'];

const COPY_FONTS_FROM_NODE_MODULES = false;
const COPY_VENDOR_FROM_NODE_MODULES = true;

const COPY_IMAGES = true;
const COPY_FONTS = true;
const COPY_VENDOR = true;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
    processCssUrls: false
});

mix.setPublicPath('resources/dist/');

if (mix.inProduction()) {

    mix.version();
    mix.disableNotifications();

    if (COPY_IMAGES) {

        mix.webpackConfig({
            plugins: [
                new CopyPlugin({
                    patterns: [{
                        from: 'resources/img', // 'img'
                        to: 'img' // 'dist/img'
                    }],
                }),
                new ImageminPlugin({
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    plugins: [
                        ImageminMozjpeg({
                            quality: 87,
                        })
                    ]
                }),
            ]
        });
    }

} else {
    if (COPY_IMAGES) {

        COUNTRIES.forEach(function (country) {
            fs.mkdirSync('resources/img/flags', {
                recursive: true
            });
            // fs.copyFileSync('node_modules/flag-icon-css/flags/4x3/' + country + '.svg', 'img/flags/' + country + '.svg');
            mix.copy('node_modules/flag-icon-css/flags/4x3/' + country + '.svg', 'resources/img/flags/' + country + '.svg');
        });

        mix.copyDirectory('resources/img', 'resources/dist/img');
    }
}

if (COPY_FONTS_FROM_NODE_MODULES) {
    mix.copy('node_modules/@vetixy/circular-std/dist/index.css', 'resources/css/fonts.scss');
    mix.copyDirectory('node_modules/@vetixy/circular-std/fonts', 'resources/fonts');
} else {
    if (COPY_FONTS) {
        mix.copyDirectory('resources/fonts', 'resources/dist/fonts');
    }
}

if (COPY_VENDOR_FROM_NODE_MODULES) {
    // Fixto
    mix.copy('node_modules/fixto/dist/fixto.min.js', 'resources/vendor/fixto/fixto.min.js');
    mix.copy('node_modules/fixto/dist/fixto.js', 'resources/vendor/fixto/fixto.js');
    // Bootstrap
    mix.copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'resources/vendor/bootstrap/bootstrap.min.css');
    mix.copy('node_modules/bootstrap/dist/css/bootstrap.min.css.map', 'resources/vendor/bootstrap/bootstrap.min.css.map');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'resources/vendor/bootstrap/bootstrap.min.js');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.min.js.map', 'resources/vendor/bootstrap/bootstrap.min.js.map');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', 'resources/vendor/bootstrap/bootstrap.bundle.min.js');
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js.map', 'resources/vendor/bootstrap/bootstrap.bundle.min.js.map');
    // jQuery
    mix.copy('node_modules/jquery/dist/jquery.min.js', 'resources/vendor/jquery/jquery.min.js');
    mix.copy('node_modules/jquery/dist/jquery.min.map', 'resources/vendor/jquery/jquery.min.map');
    mix.copy('node_modules/jquery/dist/jquery.slim.min.js', 'resources/vendor/jquery/jquery.slim.min.js');
    mix.copy('node_modules/jquery/dist/jquery.slim.min.map', 'resources/vendor/jquery/jquery.slim.min.map');
    // Dropzone
    mix.copy('node_modules/dropzone/dist/min/dropzone.min.css', 'resources/vendor/dropzone/dropzone.min.css');
    mix.copy('node_modules/dropzone/dist/min/dropzone.min.js', 'resources/vendor/dropzone/dropzone.min.js');
}

// HTML pages

/*
const PAGES = fg.sync(['*.html'], {
    objectMode: true
});

PAGES.forEach(function (page) {
    mix.copy('' + page.name, 'dist/' + page.name);
});
*/

// JS

mix.babel([
    'resources/js/go_back.js',
    'resources/js/symbols_modal.js',
    'resources/js/preloader.js',
    'resources/js/main.js',
], 'resources/dist/js/app.js');

if (COPY_VENDOR) {
    mix.babel([
        'resources/vendor/fixto/fixto.js',
    ], 'resources/dist/js/vendor.js');

    mix.copy('resources/vendor/jquery/jquery.slim.min.js', 'resources/dist/vendor/jquery/jquery.slim.min.js');
    mix.copy('resources/vendor/jquery/jquery.min.js', 'resources/dist/vendor/jquery/jquery.min.js');

    mix.copy('resources/vendor/bootstrap/bootstrap.bundle.min.js', 'resources/dist/vendor/bootstrap/bootstrap.bundle.min.js');
    mix.copy('resources/vendor/bootstrap/bootstrap.bundle.min.js.map', 'resources/dist/vendor/bootstrap/bootstrap.bundle.min.js.map');
    mix.copy('resources/vendor/bootstrap/bootstrap.min.css', 'resources/dist/vendor/bootstrap/bootstrap.min.css');
    mix.copy('resources/vendor/bootstrap/bootstrap.min.css.map', 'resources/dist/vendor/bootstrap/bootstrap.min.css.map');

    mix.copy('resources/vendor/dropzone/dropzone.min.css', 'resources/dist/vendor/dropzone/dropzone.min.css');
    mix.copy('resources/vendor/dropzone/dropzone.min.js', 'resources/dist/vendor/dropzone/dropzone.min.js');
}

// CSS

mix.sass('resources/css/app.scss', 'resources/dist/css/');

/*.webpackConfig({
 module: {
     rules: [{
         test: /(\.(png|jpe?g|gif|webp)$|^((?!font).)*\.svg$)/,
         loaders: {
             loader: 'file-loader',
             options: {
                 name: '[path][name].[ext]?[hash]',
                 context: 'src',
             }
         },
     }]
 }
}); */

//mix.minify('dist/css/app.css');

/*
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);
*/

// Full API
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.preact(src, output); <-- Identical to mix.js(), but registers Preact compilation.
// mix.coffee(src, output); <-- Identical to mix.js(), but registers CoffeeScript compilation.
// mix.ts(src, output); <-- TypeScript support. Requires tsconfig.json to exist in the same folder as webpack.mix.js
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.less(src, output);
// mix.stylus(src, output);
// mix.postCss(src, output, [require('postcss-some-plugin')()]);
// mix.browserSync('my-site.test');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.babelConfig({}); <-- Merge extra Babel configuration (plugins, etc.) with Mix's default.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.when(condition, function (mix) {}) <-- Call function if condition is true.
// mix.override(function (webpackConfig) {}) <-- Will be triggered once the webpack config object has been fully generated by Mix.
// mix.dump(); <-- Dump the generated webpack config object to the console.
// mix.extend(name, handler) <-- Extend Mix's API with your own components.
// mix.options({
//   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
//   globalVueStyles: file, // Variables file to be imported in every component.
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   terser: {}, // Terser-specific options. https://github.com/webpack-contrib/terser-webpack-plugin#options
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });