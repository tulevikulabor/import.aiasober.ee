<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WooCommerce\ProductController;
use App\Http\Controllers\WooCommerce\Corcel\ProductController as CorcelProductController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ImporterController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SyncController;
use App\Http\Controllers\UploadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (auth()->check()) {
        if (auth()->user()->isAdmin()) {
            return view('home');
        } else {
            return view('index');
        }
    } else {
        return view('index');
    }
})->middleware(['locale'])->name('index');

Route::get('/home', [HomeController::class, 'index'])->middleware(['locale'])->name('home');

Route::get('/importer/reload', [ImporterController::class, 'reloadFile'])->middleware(['locale'])->name('importer.reload');

Route::prefix('exports')->group(function () {
    Route::get('/', [ExportController::class, 'index'])->name('exports.index');
    Route::get('/download/excel/' . config('importer.excel.export.public.download_url_hash'), [ExportController::class, 'downloadExcelByHash'])->name('exports.download.excel.hash');
    Route::get('/download/' . config('importer.excel.export.public.download_url_hash'), [ExportController::class, 'downloadPageByHash'])->name('exports.download.page.hash');
    Route::get('/download/excel', [ExportController::class, 'downloadExcel'])->name('exports.download.excel');
    Route::get('/download/csv', [ExportController::class, 'downloadCsv'])->name('exports.download.csv');
    Route::get('/products', [ExportController::class, 'products'])->name('exports.products');
});

Route::post('/upload/file', [UploadController::class, 'uploadFile'])->name('upload.file');

Route::prefix('imports')->group(function () {
    Route::get('/', [ImportController::class, 'index'])->name('imports.index');
    Route::get('/download/excel/{import_file}', [ImportController::class, 'downloadExcel'])->name('imports.download.excel');
    Route::get('/run/{import_file}', [ImportController::class, 'runImport'])->name('imports.run');
    Route::get('/cancel/{import_file}', [ImportController::class, 'cancelImport'])->name('imports.cancel');
    Route::get('/trash/{import_file}', [ImportController::class, 'trashImport'])->name('imports.trash');
});

Route::prefix('images')->group(function () {
    Route::get('/', [ImageController::class, 'index'])->name('images.index');
});

Route::prefix('users')->group(function () {
    Route::get('/', [UserController::class, 'index'])->name('users.index');
});

Auth::routes();

Route::prefix('woocommerce')->group(function () {
    Route::get('/', [ProductController::class, 'index'])->name('woocommerce.index');
    Route::get('/products/all', [ProductController::class, 'index'])->name('woocommerce.products.index');
    Route::get('/products/table', [ProductController::class, 'table'])->name('woocommerce.products.table');
    Route::get('/products/database', [CorcelProductController::class, 'database'])->name('woocommerce.products.database');
    Route::get('/products/download-test-excel', [ProductController::class, 'downloadTestExcel'])->name('woocommerce.products.download-test-excel');
    Route::get('/products/gravatar/{hash}.png', [ProductController::class, 'gravatar'])->name('woocommerce.products.gravatar');
    Route::get('/products/sku/{sku}', [ProductController::class, 'filterBySKU']);
    Route::get('/products/update/{product_id}', [ProductController::class, 'update'])->middleware(['throttle:20,1']);
    Route::get('/products/count', [ProductController::class, 'count']);
    Route::get('/products/create', [ProductController::class, 'create'])->middleware(['throttle:10,1']);
    Route::get('/products/{product_id}', [ProductController::class, 'show']);
});

Route::prefix('api')->group(function () {
    Route::get('/sync-sessions/active-session', [SyncController::class, 'getActiveSession'])->name('api.sync-sessions.active-session');
    Route::get('/sync-sessions/active-session/progress', [SyncController::class, 'getProgress'])->name('api.sync-sessions.active-session.progress');
    Route::get('/import-sessions/active-session/progress', [ImportController::class, 'getProgress'])->name('api.import-sessions.active-session.progress');
});
