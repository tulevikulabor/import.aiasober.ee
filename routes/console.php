<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\Models\ImportImage;

//use SimpleXLSX;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('import:parse', function () {
    require_once __DIR__.'/../vendor/shuchkin/simplexlsx/src/SimpleXLSX.php';

    echo '<h1>Parse books.xslx</h1><pre>';
    if ($xlsx = SimpleXLSX::parse(storage_path('app/test/test_excel.xlsx'))) {
        print_r($xlsx->rows());
    } else {
        echo SimpleXLSX::parseError();
    }
    echo '<pre>';
})->purpose('Parse import excel (.xlsx)');

Artisan::command('hash:make {string}', function () {
    $this->comment('String: ' . $this->argument('string'));
    $this->comment('Hash: ' . Hash::make($this->argument('string')));
    $this->comment('Encrypt: ' . encrypt($this->argument('string')));
})->purpose('Make hash');

/* if (!function_exists('getImagesPayload')) {
    function getImagesPayload($data)
    {
        $images_string = explode(",", $data['pildid']);
        $images = [];
        $position = 0;
        foreach ($images_string as $image) {
            $import_image = ImportImage::where('filename', trim($image))->first();
            if ($import_image) {
                $image_url = asset('imported_images/' . basename($import_image->path) . '/' . $import_image->filename);
                if (check_remote_file(trim($image_url))) {
                    $images[] = [
                        'alt' => $data['nimetus_est'],
                        'name' => $import_image->filename,
                        'src' => trim($image_url),
                        'position' => $position,
                    ];
                    $position++;
                } else {
                    Log::channel('slack')->error('PostProduct error - Product (SKU: ' . $data['tootekood'] . ') image URL not exists', collect($images_string)->toArray());
                }
            }
        }
        return $images;
    }
}

Artisan::command('image:payload', function () {
    $data = [];
    $data['pildid'] = '123456.jpg, 123456.1.jpg, 123456.2.jpg';
    $data['nimetus_est'] = 'Test';
    dd(getImagesPayload($data));
})->purpose('Image payload test'); */
