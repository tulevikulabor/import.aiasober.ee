# import.aiasober.ee

## Queue worker

https://pm2.keymetrics.io/docs/usage/pm2-doc-single-page/

https://medium.com/@koonwaik/laravel-queue-using-nodejs-pm2-2e27daeea63c

https://gist.github.com/vector-kerr/a8a680f8147c71d4b7b14e697dd94f34#file-laravel-queue-worker-pm2-yml

https://subashbasnet.com.np/how-to-run-and-monitor-laravel-queue-using-pm2/

### Zone.ee

pm2 start /data03/virt4826/domeenid/www.aiasober.ee/import --max-memory-restart 2M

pm2 start laravel-queue-worker.dev.pm2.yml
pm2 start laravel-queue-worker-2.dev.pm2.yml

```sh
if [ "$(pm2 id appname) "= "[]" ]; then
    pm2 start appname
else
    pm2 reload appname
fi
```

## Commands

### Queue work

```sh
$ php artisan queue:work --queue=categories,tags,attributes,terms,products,default

$ php artisan queue:work --queue=categories,tags,attributes,terms,products,default --once
```

Use something like supervisor to watch your workers. And create your worker instances thoughtfully. See supervisor configuration part of laravel documentation.

If you insist to use cron and queue workers together, use queue:work --once to let your worker know when to stop :)

https://medium.com/@elhardoum/automatically-start-node-js-server-on-system-restarts-cab3d2194674

### Sync data

```sh
$ php artisan sync:categories --lang=et
$ php artisan sync:categories --lang=en
$ php artisan sync:categories --lang=ru

$ php artisan sync:attributes --lang=et
$ php artisan sync:attributes --lang=en
$ php artisan sync:attributes --lang=ru

$ php artisan sync:tags --lang=et
$ php artisan sync:tags --lang=en
$ php artisan sync:tags --lang=ru

$ php artisan sync:products

$ php artisan sync:products-related
```

### Job Batching

https://www.oulub.com/en-US/Laravel/queues-job-batching

https://ahmedash.com/p/laravel-jobs-batching

## Import Excel file URL

https://www.dropbox.com/scl/fi/e39eere3tbpp4yedllrn9/Spetsialistide-graafik-2020.xlsx?dl=0&rlkey=v0h9l610p3wqzvdynl0rctxn1

https://ucb1fadd8efc42a90ffb92afa6db.dl.dropboxusercontent.com/cd/0/get/BIxNetpusOxr88oELjMhf-GuAWkF8tBE_w_BZ7N8F4cihVJ7BPo0iEct_Vx9UhTpfAJyEoB3FTgQcIx_Q0Pc3-gdllH2uXxppLVzwNzjhsBSxOIv423PP7l4PKduRS3i3Xk/file?dl=1

$client = new Guzzle(['http_errors' => false]);
$response = $client->request('GET', $url);
echo $response->getStatusCode();

## Excel

shuchkin/simplexlsx

https://medium.com/@haseeb.basil/how-can-php-import-excel-to-mysql-using-an-php-xlsx-reader-and-excel-xlsx-converter-b013c31ba2cf

spatie/simple-excel

## Tunr array around 90 degrees

https://stackoverflow.com/questions/30087158/how-can-i-rotate-a-2d-array-in-php-by-90-degrees

## Dropzone

https://codepen.io/nekobog/pen/JjoZvBm

https://www.dropzonejs.com/bootstrap.html

https://github.com/dropzone/dropzone/wiki/Make-the-whole-body-a-dropzone

## Calendar

https://docs.dhtmlx.com/scheduler/samples/03_extensions/03_agenda_view.html

https://vuetifyjs.com/en/components/calendars/#api

https://pusher.com/tutorials/calendar-vue

https://tallent.us/vue-simple-calendar/

https://www.bestjquery.com/?qL64jzeo

## Preloader

https://kingrayhan.medium.com/create-a-simple-preloader-with-jquery-c6efb03c7f23

https://icons8.com/preloaders/

## Mime types

https://github.com/hhsadiq/laravel-mime-type

## Woocommerce API

```php
$params = [
   'per_page' => 100,
   'page' => 1
];


$woocommerceorder = Woocommerce::get('orders', $params);
while (Woocommerce::hasNextPage()) {
         $params = [
            'per_page' => 100,
            'page' => Woocommerce::nextPage()
         ];
         $woocommerceorderpart = Woocommerce::get('orders', $params);
         $woocommerceorder = array_merge($woocommerceorder, $woocommerceorderpart);
}
        ```

```json     
"translations": {
    "en": "42859",
    "ru": "50173"
},
"lang": "et",
```


### Manage stock

#### POST

```json  
"manage_stock": {
    "required": false,
    "default": false,
    "description": "Stock management at product level.",
    "type": "boolean"
},
"stock_quantity": {
    "required": false,
    "description": "Stock quantity.",
    "type": "integer"
},
"in_stock": {
    "required": false,
    "default": true,
    "description": "Controls whether or not the product is listed as \"in stock\" or \"out of stock\" on the frontend.",
    "type": "boolean"
},
```