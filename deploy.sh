#!/bin/sh

pm2 stop all

php artisan down

git pull origin master

if [ ! -e .env ]; then
    echo ".env file does not exist"
    cp -v .env.example .env
    php artisan key:generate
else
    echo ".env file exists"
fi

composer install --no-interaction --prefer-dist --optimize-autoloader

composer theme:symlink
composer assets:symlink
php artisan storage:link

php artisan cache:clear
php artisan config:clear
php artisan config:cache

php artisan migrate --force

php artisan up

pm2 restart all
php artisan queue:restart