<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Locales
    |--------------------------------------------------------------------------
    |
    | These values are used in language navigation and validation.
    |
    */

    'locales' => [

         'en' => [
              'alias' => 'ENG',
              'locale' => 'en_GB',
              'language' => 'en',
              'country' => 'gb',
         ],

         'ee' => [
              'alias' => 'EST',
              'locale' => 'et_EE',
              'language' => 'et',
              'country' => 'ee',
         ],

         /*'ru' => [
              'alias' => 'RUS',
              'locale' => 'ru_RU',
              'country' => 'ru',
         ],

         'fi' => [
              'alias' => 'FIN',
              'locale' => 'fi_FI',
              'country' => 'fi',
         ],*/
    ],

];
