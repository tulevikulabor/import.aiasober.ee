<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Import / Export Excel configuration
    |--------------------------------------------------------------------------
    */
   
    'image' => [
        'size' => [
            'width' => 1024,
            'height' => 1024,
        ],
        'quality' => 90,
    ],

    'excel' => [
        
        /*
        |--------------------------------------------------------------------------
        | Import: Excel filename
        |--------------------------------------------------------------------------
        */
        
        'url' => env('EXCEL_FILE_URL', ''),
        
        'name' => env('EXCEL_FILE_NAME', 'import.xlsx'),
        
        'import' => [
            
            /*
            |--------------------------------------------------------------------------
            | Import: For testing
            |--------------------------------------------------------------------------
            */
            
            'test' => [
                'filename' => [
                    'csv' => 'import-test-aiasober-ee-export-2021-03-08_00-20-27.csv',
                    //'excel' => 'import-test-aiasober-ee-export-2021-03-08_00-20-27.xlsx',
                    'excel' => 'import-aiasober-ee-test-export-2021-03-03_15-25-34_lembit.xlsx',
                ],
                'limited' => [
                    'filename' => [
                        'csv' => 'import-aiasober-ee-test-export-2021-02-24_12-42-30___LIMIT-10.csv',
                        'excel' => 'import-aiasober-ee-test-export-2021-02-24_12-42-21___LIMIT-10.xlsx',
                    ],
                ],
            ],
        ],
        
        /*
        |--------------------------------------------------------------------------
        | Export: Excel / CSV filename
        |--------------------------------------------------------------------------
        */
        
        'export' => [
            'filename' => str_replace('.', '-', parse_url(env('APP_URL'), PHP_URL_HOST)). '-export-' . date('Y-m-d_H-i-s'),
            'public' => [
                "download_url_hash" => 'mT8Bb4N6GyLXVPHZdpUMmtdizic9Cq9Wp9tVpBKWp1tSfxjTco',
            ],
        ],
        
        /*
        |--------------------------------------------------------------------------
        | Excel / CSV headings
        |--------------------------------------------------------------------------
        */
        
        'headings' => [
            'sku' => 'Tootekood',
            'latin_name' => 'Nimetus (LAT)',
            'name_et' => 'Nimetus (EST)',
            'name_en' => 'Nimetus (ENG)',
            'name_ru' => 'Nimetus (RUS)',
            'status' => 'Staatus',
            'categories' => 'Kategooriad',
            'upsell_skus' => 'Seotud tootekoodid',
            'tags' => 'Tingmärgid',
            'stock_quantity' => 'Laoseis',
            'price' => 'Hind',
            //'regular_price' => 'Tavahind',
            'attribute_1' => 'Omadus-1',
            'attribute_1_value' => 'Väärtus-1',
            'attribute_2' => 'Omadus-2',
            'attribute_2_value' => 'Väärtus-2',
            'attribute_3' => 'Omadus-3',
            'attribute_3_value' => 'Väärtus-3',
            'attribute_4' => 'Omadus-4',
            'attribute_4_value' => 'Väärtus-4',
            'species_description' => 'Liigikirjeldus',
            'notes_et' => 'Märkused ja lisainfo (EST)',
            'notes_en' => 'Märkused ja lisainfo (ENG)',
            'notes_ru' => 'Märkused ja lisainfo (RUS)',
            'description' => 'Kirjeldus',
            'images' => 'Pildid',
            'images_full' => 'Piltide lingid',
            //'id' => '#',
            //'external_id' => 'ID',
        ],
        
        /*
        |--------------------------------------------------------------------------
        | Tags
        |--------------------------------------------------------------------------
        | EST: Tingmärgid
        | ENG: Conventional signs
        | RUS: Условные знаки
        */
        
        'tags' => [
            'pä' => 'Päikeseline kasvukoht',        // OK - ??? 'Päikeselisele kasvukohale',
            'pv' => 'Poolvarjuline kasvukoht',      // OK - ??? 'Poolvarjulisele kasvukohale',
            'tv' => 'Varjuline kasvukoht',          // OK - ??? 'Täisvarjulisele kasvukohale',
            'ku' => 'Kuivale pinnasele',            // OK - ??? 'Kuivemale pinnasele',
            'pn' => 'Parasniiskele pinnasele',      // OK
            'ni' => 'Niiskele pinnasele',           // OK
            'll' => 'Sobib lõikelilleks',           // OK
            'lõ' => 'Lõhnavad õied või lehestik',   // OK
            'po' => 'Sobib potitaimeks',            // OK
            'am' => 'Sobib amplisse',               // OK
            'pt' => 'Patenteeritud sort',           // OK
            'ld' => 'Lehtdekoratiivne',             // OK
            'pm' => 'Putukaid meelitav',            // OK
            'ms' => 'Mittesöödav',                  // OK
        ],
        
        /*
        |--------------------------------------------------------------------------
        | Type
        |--------------------------------------------------------------------------
        */
        
        'status' => [
            'simple' => 'Lihtne',
            'grouped' => 'Grupeeritud',
            'external' => 'Väline',
            'variable' => 'Variatsioon',
        ],
        
        /*
        |--------------------------------------------------------------------------
        | Status
        |--------------------------------------------------------------------------
        */
        
        'status' => [
            'publish' => 'Nähtav', // Avaldatud
            'private' => 'Peidetud', // Privaatne
            'pending' => 'Ootel', // Läbivaatamise ootel
            'draft' => 'Mustand', // Mustand
            'trash' => 'Prügikastis', // Prügi (NB! Not officially supported)
        ],
        
        /*
        |--------------------------------------------------------------------------
        | Catalog visibility
        |--------------------------------------------------------------------------
        */
        
        'catalog_visibility' => [
            'visible' => 'Avalik', // Nähtavus kataloogis: "Pood ja otsingutulemused"
            'catalog' => 'Pood', // Nähtavus kataloogis: "Ainult pood"
            'search' => 'Otsing', // Nähtavus kataloogis: Ainult otsingutulemused
            'hidden' => 'Peidetud', // Nähtavus kataloogis: Peidetud
        ],
        
        /*
        |--------------------------------------------------------------------------
        | Stock status
        |--------------------------------------------------------------------------
        */
        
        'stock_status' => [
            'instock' => 'Laos',
            'outofstock' => 'Ei ole laos',
        ],
        
        /*
        |--------------------------------------------------------------------------
        | Featured
        |--------------------------------------------------------------------------
        */
        
        'featured' => [
            'true' => 'Jah',
            'false' => 'Ei',
        ],
        
        /*
        |--------------------------------------------------------------------------
        | Excel: headings --> meta_data
        |--------------------------------------------------------------------------
        */
        
        'meta_data' => [
            'latin_name' => 'latin_name',
            'notes' => 'markused_ja_lisainfo',
            'species_description' => 'liigikirjeldus',
        ],
        
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Import: File mime types
    |--------------------------------------------------------------------------
    */
   
    'mime' => [
        'image' => [
            'image/jpeg', // .jpg, .jpeg, .jfif, .pjpeg, .pjp
            'image/png', // .png
        ],
        'excel' => [
            'application/vnd.ms-excel', // .xls
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // .xlsx
            'text/csv', // .csv
            'application/csv', // .csv
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Import: File storage paths
    |--------------------------------------------------------------------------
    */
   
    'storage' => [
        'image' => storage_path('app/private/imported/images'),
        'excel' => storage_path('app/private/imported/excel'),
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Languages
    |--------------------------------------------------------------------------
    */
 
    'language' => [
        'default' => 'et',
        'list' => ['et', 'en', 'ru'],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Import: WooCommerce API configuration
    |--------------------------------------------------------------------------
    | Used onfy for importing.
    |--------------------------------------------------------------------------
    | Importing:
    |  - Config: /config/importer.php
    |  - Client: use Automattic\WooCommerce\Client;
    |--------------------------------------------------------------------------
    | Exporting:
    |  - Config: /config/oocommerce.php
    |  - Client: use Codexshaper\WooCommerce\Facades\Product;
    */
    
    'wooocommerce' => [
        'api' => [
            'store_url' => env('IMPORT_WOOCOMMERCE_STORE_URL', 'YOUR_STORE_URL'),
            'consumer_key' => env('IMPORT_WOOCOMMERCE_CONSUMER_KEY', 'YOUR_CONSUMER_KEY'),
            'consumer_secret' => env('IMPORT_WOOCOMMERCE_CONSUMER_SECRET', 'YOUR_CONSUMER_SECRET'),
            'verify_ssl' => env('IMPORT_WOOCOMMERCE_VERIFY_SSL', false),
            'api_version' => env('IMPORT_WOOCOMMERCE_API_VERSION', 'v3'),
            'wp_api' => env('IMPORT_WP_API_INTEGRATION', true),
            'query_string_auth' => env('IMPORT_WOOCOMMERCE_WP_QUERY_STRING_AUTH', false),
            'timeout' => env('IMPORT_WOOCOMMERCE_WP_TIMEOUT', 15),
            'header_total' => env('IMPORT_WOOCOMMERCE_WP_HEADER_TOTAL', 'X-WP-Total'),
            'header_total_pages' => env('IMPORT_WOOCOMMERCE_WP_HEADER_TOTAL_PAGES', 'X-WP-TotalPages'),
        ],
    ],
];
