<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'username' => 'raido',
            'name' => 'Raido',
            'first_name' => 'Raido',
            'last_name' => 'Orumets',
            'email' => 'raido@tulevikulabor.ee',
            'is_admin' => true,
            'is_superuser' => true,
            'password' => Hash::make('AiaFriend123'),
            'avatar' => 'https://cravatar.eu/avatar/raido/50.png',
            'locale' => 'ee',
        ]);

        User::firstOrCreate([
            'username' => 'rauli',
            'name' => 'Rauli',
            'first_name' => 'Rauli',
            'last_name' => 'Orumets',
            'email' => 'rauli@tulevikulabor.ee',
            'is_admin' => true,
            'is_superuser' => false,
            'password' => Hash::make('AiaFriend'),
            'avatar' => 'https://cravatar.eu/avatar/rauli/50.png',
            'locale' => 'ee',
        ]);

        User::firstOrCreate([
            'username' => 'lembit',
            'name' => 'Lembit',
            'first_name' => 'Lembit',
            'last_name' => 'Kaarna',
            'email' => 'lembit@aiasober.ee',
            'is_admin' => true,
            'is_superuser' => false,
            'password' => Hash::make('AiaFriend'),
            'avatar' => 'https://cravatar.eu/avatar/lembit/50.png',
            'locale' => 'ee',
        ]);
    }
}
