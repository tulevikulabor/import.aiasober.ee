<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSyncSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sync_sessions', function (Blueprint $table) {
            $table->id();
            $table->timestamp('started_at')->nullable()->comment('Session started at');
            $table->timestamp('page_changed_at')->nullable()->comment('Session page changed at');
            $table->timestamp('finished_at')->nullable()->comment('Session finished at');
            $table->integer('products')->nullable()->comment('Total products');
            $table->integer('pages')->nullable()->comment('Page');
            $table->integer('page')->nullable()->comment('Current page');
            $table->text('export_file')->nullable()->comment('Path to export file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sync_sessions');
    }
}
