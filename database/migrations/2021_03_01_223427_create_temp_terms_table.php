<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_terms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('external_id')->index()->nullable()->comment('WooCommerce term ID');
            $table->foreignId('attribute_external_id')->index()->nullable()->comment('WooCommerce attribute ID');
            $table->char('language', 2)->default(config('importer.language.default'))->nullable()->comment('WooCommerce term language (language not included in payload)');
            $table->text('name')->nullable()->comment('WooCommerce term name');
            $table->text('slug')->nullable()->comment('WooCommerce term slug');
            $table->longText('description')->nullable()->comment('WooCommerce term description');
            $table->integer('menu_order')->nullable()->comment('WooCommerce term order');
            $table->integer('count')->nullable()->comment('WooCommerce term products count');
            $table->longText('payload')->nullable(); // json
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_terms');
    }
}
