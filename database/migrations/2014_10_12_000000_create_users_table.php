<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username', 50)->unique()->default('')->comment('Username, default e-mail');
            $table->string('name')->nullable()->comment('User first, middle and last name or nickname');
            $table->string('first_name')->nullable()->comment('User first name');
            $table->string('last_name')->nullable()->comment('User last name');
            $table->string('email', 50)->unique()->comment('User e-mail');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable()->comment('User password');
            $table->string('avatar')->nullable()->comment('User avatar image URL');
            $table->string('phone')->nullable()->comment('User phone number');
            $table->char('locale', 2)->default('en')->comment('User default locale');
            $table->rememberToken()->comment('User login remember token');
            $table->boolean('is_admin')->default(false)->comment('User is "admin": true/false');
            $table->integer('logins')->default(0)->comment('User login count');
            $table->datetime('first_login')->nullable()->comment('User first login date and time');
            $table->datetime('last_login')->nullable()->comment('User last login date and time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
