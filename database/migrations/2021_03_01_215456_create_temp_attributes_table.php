<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_attributes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('external_id')->index()->nullable()->comment('WooCommerce attribute ID');
            $table->char('language', 2)->default(config('importer.language.default'))->nullable()->comment('WooCommerce attribute language (language not included in payload)');
            $table->string('name')->nullable()->comment('WooCommerce attribute name');
            $table->string('slug')->nullable()->comment('WooCommerce attribute slug');
            $table->string('type')->nullable()->comment('WooCommerce attribute type');
            $table->string('order_by')->nullable()->comment('WooCommerce attribute order by');
            $table->boolean('has_archives')->nullable()->comment('WooCommerce attribute has archives');
            $table->longText('payload')->nullable(); // json
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_attributes');
    }
}
