<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_files', function (Blueprint $table) {
            $table->id();
            $table->text('filename')->nullable()->comment('New filename');
            $table->text('original_filename')->nullable()->comment('Original filename');
            $table->foreignId('user_id')->nullable()->index()->comment('Owner of the import');
            $table->enum('state', ['pending', 'active', 'inactive', 'file_error'])->default('pending')->comment('Import file state');
            $table->string('job_batch_id')->nullable()->comment('Job batch ID');
            $table->timestamp('job_batch_started_at')->nullable()->comment('Timestamp of the job batch started');
            $table->timestamp('job_batch_finished_at')->nullable()->comment('Timestamp of the job batch finished');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_files');
    }
}
