<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('external_id')->index()->nullable();
            $table->string('sku', 30)->index()->nullable();
            $table->char('language', 2)->nullable();
            $table->string('status', 20)->nullable();
            $table->string('catalog_visibility', 20)->nullable();
            $table->string('latin_name', 100)->nullable();
            $table->string('name')->nullable();
            $table->text('slug')->nullable();
            $table->text('description')->nullable();
            $table->text('short_description')->nullable();
            $table->double('price')->nullable();
            $table->double('regular_price')->nullable();
            $table->double('sale_price')->nullable();
            $table->integer('stock_quantity')->nullable();
            $table->string('stock_status', 20)->nullable();
            $table->text('permalink')->nullable();
            $table->text('categories')->nullable(); // json
            $table->text('related_ids')->nullable();  // json
            $table->text('related_skus')->nullable(); // json
            $table->text('upsell_ids')->nullable(); // json
            $table->text('upsell_skus')->nullable(); // json
            $table->text('tags')->nullable(); // json
            $table->text('images')->nullable(); // json
            $table->string('attribute_1')->nullable();
            $table->text('attribute_1_value')->nullable();
            $table->string('attribute_2')->nullable();
            $table->text('attribute_2_value')->nullable();
            $table->string('attribute_3')->nullable();
            $table->text('attribute_3_value')->nullable();
            $table->string('attribute_4')->nullable();
            $table->text('attribute_4_value')->nullable();
            $table->text('notes')->nullable();
            $table->text('species_description')->nullable();
            $table->text('species_url')->nullable();
            $table->longText('payload')->nullable(); // json
            $table->timestamp('date_created')->nullable();;
            $table->timestamp('date_modified')->nullable();;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_products');
    }
}
