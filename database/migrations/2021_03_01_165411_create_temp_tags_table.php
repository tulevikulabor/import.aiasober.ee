<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_tags', function (Blueprint $table) {
            $table->id();
            $table->foreignId('external_id')->index()->nullable()->comment('WooCommerce tag ID');
            $table->char('language', 2)->default(config('importer.language.default'))->nullable()->comment('WooCommerce tag language (language not included in payload)');
            $table->string('name')->nullable()->comment('WooCommerce tag name');
            $table->string('short_name', 10)->nullable()->comment('Importer short name (Example: pä, pv, ...)');
            $table->text('slug')->nullable()->comment('WooCommerce tag slug');
            $table->text('description')->nullable()->comment('WooCommerce tag description');
            $table->integer('count')->nullable()->comment('WooCommerce tag products count');
            $table->longText('payload')->nullable(); // json
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_tags');
    }
}
