<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_images', function (Blueprint $table) {
            $table->id();
            $table->string('sku', 30)->index()->nullable()->comment('Product SKU');
            $table->text('path')->nullable()->comment('Image path');
            $table->string('filename')->index()->nullable()->comment('Image filename');
            $table->integer('order')->nullable()->comment('Image order');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_images');
    }
}
