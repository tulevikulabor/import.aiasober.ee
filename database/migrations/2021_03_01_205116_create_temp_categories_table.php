<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_categories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('external_id')->index()->nullable()->comment('WooCommerce category ID');
            $table->foreignId('parent_external_id')->index()->nullable()->comment('WooCommerce parent category ID');
            $table->char('language', 2)->default(config('importer.language.default'))->nullable()->comment('WooCommerce category language (language not included in payload)');
            $table->text('name')->nullable()->comment('WooCommerce category name');
            $table->text('slug')->nullable()->comment('WooCommerce category slug');
            $table->longText('description')->nullable()->comment('WooCommerce category description');
            $table->string('display')->nullable()->comment('WooCommerce category display');
            $table->text('image')->nullable()->comment('WooCommerce category image'); // json
            $table->integer('menu_order')->nullable()->comment('WooCommerce category order');
            $table->integer('count')->nullable()->comment('WooCommerce category products count');
            $table->longText('payload')->nullable(); // json
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_categories');
    }
}
