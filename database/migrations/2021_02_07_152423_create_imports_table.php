<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imports', function (Blueprint $table) {
            $table->id();
            $table->enum('source', ['storage', 'upload', 'terminal', 'wordpress_api', 'cloud', 'google_drive', 'one_drive', 'dropbox', 'url'])->nullable()->comment('Source of the import');
            $table->foreignId('import_file_id')->nullable()->comment('Import file');
            $table->text('file')->nullable()->comment('Source file name, path or URL');
            $table->foreignId('user_id')->nullable()->index()->comment('Owner of the import');
            $table->foreignId('state_user_id')->nullable()->index()->comment('User that triggered a import state');
            $table->enum('state', ['uploaded', 'pending', 'started', 'paused', 'finished', 'terminated', 'error'])->default('pending')->comment('Import state');
            $table->timestamp('uploaded_at')->nullable()->comment('Timestamp of the uploaded source file');
            $table->timestamp('started_at')->nullable()->comment('Timestamp of the start');
            $table->timestamp('paused_at')->nullable()->comment('Timestamp of the temporary pause');
            $table->timestamp('finished_at')->nullable()->comment('Timestamp of the finish');
            $table->timestamp('terminated_at')->nullable()->comment('Timestamp of the termination, error or cancellation');
            $table->json('errors')->nullable()->comment('Import errors');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imports');
    }
}
