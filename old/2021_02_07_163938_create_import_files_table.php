<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_files', function (Blueprint $table) {
            $table->id();
            $table->enum('source', ['storage', 'upload', 'terminal', 'wordpress_api', 'cloud', 'google_drive', 'one_drive', 'dropbox', 'url'])->nullable()->comment('Source of the import file');
            $table->text('file')->nullable()->comment('Source file name, path or URL');
            $table->foreignId('user_id')->nullable()->index()->comment('Owner of the import');
            $table->foreignId('download_user_id')->nullable()->index()->comment('Last User that downloaded import file');
            $table->timestamp('uploaded_at')->nullable()->comment('Timestamp of the uploaded source file');
            $table->timestamp('downloaded_at')->nullable()->comment('Timestamp of the downloaded source file');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_files');
    }
}
