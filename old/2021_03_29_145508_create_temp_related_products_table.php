<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempRelatedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_related_products', function (Blueprint $table) {
            $table->id();
            $table->enum('relation_type', ['related', 'upsell'])->nullable()->comment('Relation name: related, upsell');
            $table->foreignId('external_id')->index()->nullable()->comment('Product ID');
            $table->string('sku', 30)->index()->nullable()->comment('Product SKU');
            $table->foreignId('related_external_id')->index()->nullable()->comment('Related product ID');
            $table->string('related_sku', 30)->index()->nullable()->comment('Related product SKU');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_related_products');
    }
}
