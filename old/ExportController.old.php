<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\SimpleExcel\SimpleExcelWriter;
use App\Models\TempProduct;
use App\Exports\ProductsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\SyncSession;
use Cache;

class ExportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    
    public function index(SyncSession $sync_session)
    {
        $sync_sessions = $sync_session->orderBy('id', 'desc')->paginate(10);
                
        return view('export.index')->with(['sync_sessions' => $sync_sessions]);
    }
    
    public function download2()
    {
        $products = Cache::rememberForever('products', function () {
            $products = [];
            $products['et'] = TempProduct::where('language', 'et')->limit(50)->get();
            $products['ru'] = TempProduct::where('language', 'ru')->limit(50)->get();
            $products['en'] = TempProduct::where('language', 'en')->limit(50)->get();
            
            return $products;
        });
        
        //$products = $products->toArray();
        
        $array = [];
        
        foreach ($products['et'] as $product) {
            $product_en = $this->getProductBySku($products['en'], $product->sku);
            $product_ru = $this->getProductBySku($products['ru'], $product->sku);
            
            $array[] = [
                'id' => $product->external_id,
                'sku' => $product->sku,
                'latin_name' => $product->latin_name,
                'name_et' => $product->name,
                'name_en' => $product_en->name,
                'name_ru' => $product_ru->name,
                'status' => 'Aktiivne', //$product_ru->status,
                'categories' => implode('; ', array_unique(json_decode($product->categories))),
                'related' => implode('; ', array_unique(json_decode($product->related_ids))),
                'tags' => implode('; ', array_unique(json_decode($product->tags))),
                'stock_quantity' => $product->stock_quantity,
                'price' => $product->price,
                'regular_price' => $product->regular_price,
                'attribute_1' => $product->attribute_1,
                'attribute_1_value' => $product->attribute_1_value,
                'attribute_2' => $product->attribute_2,
                'attribute_2_value' => $product->attribute_2_value,
                'attribute_3' => $product->attribute_3,
                'attribute_3_value' => $product->attribute_3_value,
                'attribute_4' => $product->attribute_4,
                'attribute_4_value' => $product->attribute_4_value,
                'species_description_et' => $product->species_description,
                'species_description_en' => $product_en->species_description,
                'species_description_ru' => $product_ru->species_description,
                'notes' => $product->notes,
                'description_et' => $product->description,
                'description_en' => $product_en->description,
                'description_er' => $product_ru->description,
                'images' => implode('; ', array_unique(json_decode($product->images))),
            ];
        }
        
        $writer = SimpleExcelWriter::streamDownload('aiasober-ee-export.xlsx')->addRows($array)->toBrowser();
    }
    
    public function download()
    {
        // $products = Cache::remember('products_export', 60, function () {
        $products = (new TempProduct)->exportQuery()->get();
        //    return $products;
        // });
        
        $writer = SimpleExcelWriter::streamDownload('aiasober-ee-export.csv')->addRows($products->toArray())->toBrowser();
    }
    
    public function getProductBySku($products, $sku)
    {
        foreach ($products as $product) {
            if ($product->sku == $sku) {
                break;
            }
        }
        return $product;
    }
    
    public function exportExcel()
    {
        return (new ProductsExport)->download('aiasober-ee-export.xlsx');
    }
}
