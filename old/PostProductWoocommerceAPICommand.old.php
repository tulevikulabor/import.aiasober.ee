<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;
use Automattic\WooCommerce\Client;
use Codexshaper\WooCommerce\Facades\Product;
use Illuminate\Support\Str;
use App\Support\Timer;

class PostProductWoocommerceAPICommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wc:post-product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post product to WooCommerce API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Timer::start('post_products_command');
        
        $file = storage_path('app/test/' . config('importer.excel.import.test.limited.filename.excel'));

        /* $woocommerce = new Client(
            config('importer.wooocommerce.api.store_url'),
            config('importer.wooocommerce.api.consumer_key'),
            config('importer.wooocommerce.api.consumer_secret'),
            [
                'version' => config('importer.wooocommerce.api.api_version'),
                'timeout' => config('importer.wooocommerce.api.timeout'),
                'verify_ssl' => config('importer.wooocommerce.api.verify_ssl'),
                'query_string_auth' => config('importer.wooocommerce.api.query_string_auth'),
                'wp_api' => config('importer.wooocommerce.api.wp_api'),
            ]
        ); */
        
        $collection = (new ProductsImport)->toCollection($file);
        
        $row = $collection[0][2];
        
        $this->info(json_encode($row, JSON_PRETTY_PRINT));
        
        $categories_string = explode(";", $row['kategooriad']);
        $categories = [];
        foreach ($categories_string as $category) {
            $categories[] = [
                'slug' => Str::slug(trim($category)),
            ];
        }
        
        $images_string = explode(",", $row['piltide_lingid']);
        $images = [];
        $position = 0;
        foreach ($images_string as $image) {
            $images[] = [
                'alt' => $row['nimetus_est'],
                'name' => basename(trim($image)),
                'src' => trim($image),
                'position' => $position,
            ];
            $position++;
        }
        
        //get_gravatar
        
        $data = [
            'sku' => strval($row['sku']),
            'name' => $row['nimetus_est'],
            'status' => $row['staatus'],
            'description' => $row['kirjeldus'],
            'categories' => $categories,
            'images' => $images,
            'lang' => 'et',
            'price' => str_replace('.', ',', $row['hind']),
            'regular_price' => str_replace('.', ',', $row['tavahind']),
        ];
        
        //$product = $woocommerce->post('products', $data);
        $product = Product::where('sku', strval($row['sku']))->first();
        if (!empty($product['id'])) {
            $product = Product::update($product['id'], $data);
        } else {
            $product = Product::create($data);
        }
        
        Timer::stop('post_products_command');
        $timer = Timer::get('post_products_command');
        $time = Str::beforeLast($timer['time'], '.');
        
        $this->info(json_encode($data, JSON_PRETTY_PRINT));
        $this->comment($time);
    }
    
    //$import = Excel::toArray(new ProductsImport, $file);
    
    /* $this->output->title('Starting import');
    (new ProductsImport)->withOutput($this->output)->toArray($file);
    $this->output->success('Import successful'); */
}
