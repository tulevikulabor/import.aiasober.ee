<?php

/*
 * Locale getter and setter
 **/
if (!function_exists('locale')) {
    function locale($locale = null)
    {
        if ($locale) {
            app()->setLocale($locale);
        }
        return app()->getLocale();
    }
}

/*
 * Array to Object converter
 **/
function object($array = [])
{
    $object = new \stdClass;
    foreach ($array as $key => $value) {
        $object->$key = $value;
    }
    return $object;
}

/*
 * Bank reference number helper
 **/
function reference_number($number = false)
{

    // https://www.pangaliit.ee/arveldused/viitenumber

    if (empty($number)) {
        $number = rand(10000000, 99999999);
    }

    $multiplier = 7;
    $prime_number = $number;
    $checkmark = false;

    // Do 7-3-1 method
    do {
        // Multiplications are added
        $checkmark = $checkmark + (($number % 10) * $multiplier);

        if ($multiplier == 7) {
            $multiplier = 3;
        } elseif ($multiplier == 3) {
            $multiplier = 1;
        } elseif ($multiplier == 1) {
            $multiplier = 7;
        }
    } while ((int)$number /= 10);

    // The nearest dozen times number
    $checkmark = 10 - ($checkmark % 10);

    // If the number is 10, then the number must be 0
    if ($checkmark == 10) {
        $checkmark = 0;
    }

    $reference_number = $prime_number.$checkmark;

    return $reference_number;
}

/*
 * Trim string
 **/
function str_trim($string)
{
    return trim(preg_replace('!\s+!', ' ', $string));
}

/*
 * Clean string from special characters
 **/
function str_clean($string)
{
    return preg_replace('/[^A-Za-z0-9\'\s\-]/', '', str_trim($string));
}

/*
 * Clean string from whitespaces
 **/
function str_clean_whitespaces($string)
{
    return preg_replace('/\s+/', '', $string);
}

/*
 * Uppercase name
 **/
function ucname($name)
{
    $name = ucwords(mb_strtolower($name));

    foreach (array('-', '\'') as $delimiter) {
        if (strpos($name, $delimiter) !== false) {
            $name = implode($delimiter, array_map('ucfirst', explode($delimiter, $name)));
        }
    }
    return $name;
}

/*
 * Format, set, get name by parts
 **/
function name($first_name, $last_name = false)
{
    if (empty($first_name)) {
        return false;
    }

    $first_name = ucname(str_clean($first_name));
    $last_name = ucname(str_clean($last_name));

    if (!$last_name) {
        $parts = explode(' ', $first_name);
        $last_name = (count($parts) > 1)?$parts[count($parts)-1]:false;

        if (count($parts) > 2) {
            unset($parts[count($parts)-1]);
            $first_name = implode(' ', $parts);
        } else {
            $first_name = $parts[0];
        }
    }

    return object([
        'name'          => $first_name.' '.$last_name,
        'first_name'    => $first_name,
        'last_name'     => $last_name,
        'firstname'     => $first_name,
        'lastname'      => $last_name,
    ]);
}

/*
 * Format, set, get email by parts
 **/
function email($email)
{
    $email = mb_strtolower(str_clean_whitespaces($email));

    try {
        $at_symbols = ["(at)", "[at]", "(ät)", "[ät]", "___"]; // TODO:
        foreach ($at_symbols as $at_symbol) {
            $email = str_replace($at_symbol, '@', $email);
        }
        list($username, $host) = explode('@', $email);

        $name = str_replace('_', ' ', $username);
        $name = str_replace('.', ' ', $name);
        $name = preg_replace('/[0-9]+/', '', $name);

        $name = name($name);

        return object([
            'email'         => $email,
            'username'      => $username,
            'host'          => $host,
            'name'          => $name->name,
            'first_name'    => $name->first_name,
            'last_name'     => $name->last_name,
        ]);
    } catch (\Exception $e) {
        return object([
            'email'         => null,
            'username'      => null,
            'host'          => null,
            'name'          => null,
            'first_name'    => null,
            'last_name'     => null,
        ]);
    }
}

/*
 * Returns the form input value
 **/
function input_value($name, $default_value = false)
{
    return (old($name))?old($name):$default_value;
}

/*
 * Return text of the boolean value
 **/
function boolean($boolean)
{
    return ($boolean) ? __('Yes') : __('No');
}

function read_more($string, $chars = 100)
{
    $string = strip_tags($string);
    if (strlen($string) > $chars) {
        // truncate string
        $stringCut = substr($string, 0, $chars);
        $endPoint = strrpos($stringCut, ' ');
        //if the string doesn't contain any space then it will cut without word basis.
        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
        //$string .= '... <a href="/this/story">Read More</a>';
        $string .= '...';
    }
    
    return $string;
}


function array_remove_value($array, $value)
{
    if (($key = array_search($value, $array)) !== false) {
        unset($array[$key]);
    }
    return array_values($array);
}

/**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string $email The email address
 * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
 * @param string $d Default imageset to use [ 404 | mp | identicon | monsterid | wavatar ]
 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
 * @param boole $img True to return a complete IMG tag False for just the URL
 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
 * @return String containing either just a URL or a complete image tag
 * @source https://gravatar.com/site/implement/images/php/
 */
function get_gravatar($email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = array())
{
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5(strtolower(trim($email)));
    $url .= "?s=$s&d=$d&r=$r";
    if ($img) {
        $url = '<img src="' . $url . '"';
        foreach ($atts as $key => $val) {
            $url .= ' ' . $key . '="' . $val . '"';
        }
        $url .= ' />';
    }
    return $url;
}


function check_remote_file($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // don't download content
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    curl_close($ch);
    if ($result !== false) {
        return true;
    } else {
        return false;
    }
}
