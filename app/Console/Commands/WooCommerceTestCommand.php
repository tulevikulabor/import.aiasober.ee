<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Commands\WooCommerceCountCommand;

class WooCommerceTestCommand extends WooCommerceCountCommand
{
    protected $signature = 'wc:test';

    protected $description = 'WooCommerce API connection test';
}
