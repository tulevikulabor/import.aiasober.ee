<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SyncCategories;
use Carbon\Carbon;
use Codexshaper\WooCommerce\Facades\Category;

//use App\Events\SyncCategoriesStarted;
//use App\Events\SyncCategoriesFinished;

class SyncCategoriesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:categories {--page=1 : Page} {--lang=et : Language (et, en, ru, all)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync categories from WooCommerce API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $page = $this->option('page');
        $lang = $this->option('lang');
        
        $categories = Category::paginate(100, strval($page), ['lang' => $lang]);
        
        SyncCategories::dispatch($categories['data'], $categories['meta'], $lang)->onQueue('categories');
    }
}
