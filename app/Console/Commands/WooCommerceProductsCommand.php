<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Codexshaper\WooCommerce\Facades\Product;

class WooCommerceProductsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wc:products {page : Page}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'WooCommerce Products by pages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $page = $this->argument('page');
        
        $products = Product::paginate(5, ($page > 0) ? $page : strval(1));
        
        $this->info(json_encode($products, JSON_PRETTY_PRINT));
    }
}
