<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SyncTerms;
use App\Models\SyncSession;
use Carbon\Carbon;
use Codexshaper\WooCommerce\Facades\Term;

//use App\Events\SyncTermsStarted;
//use App\Events\SyncTermsFinished;

class SyncTermsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:terms {attribute_id : Attribute ID} {--page=0 : Page} {--lang=et : Language (et, en, ru, all)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync terms from WooCommerce API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $attribute_id = $this->argument('attribute_id');
        $page = intval($this->option('page'));
        $lang = $this->option('lang');
        
        if ($page > 0) {
            $terms = Term::paginate($attribute_id, 100, strval($page), ['lang' => $lang]);
            SyncTerms::dispatch($attribute_id, $terms['data'], $terms['meta'], $lang)->onQueue('terms');
        } else {
            $next_page = 1;
            
            while ($next_page) {
                $terms = Term::paginate($attribute_id, 100, strval($next_page), ['lang' => $lang]);
                $next_page = $terms['meta']['next_page'];
                SyncTerms::dispatch($attribute_id, $terms['data'], $terms['meta'], $lang)->onQueue('terms');
            }
        }
    }
}
