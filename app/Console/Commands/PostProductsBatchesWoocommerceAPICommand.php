<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Str;
use Illuminate\Bus\Batch;
use App\Support\Timer;
use App\Imports\ProductsImport;
use App\Http\WooCommerce\Requests\PostProduct;
use App\Jobs\PostProducts;
use App\Models\ImportFile;
use Throwable;

class PostProductsBatchesWoocommerceAPICommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wc:post-products-batches {--size=10 : Products in batch} {--rows= : Total Excel rows (for testing purpose)} {--import_file_id= : Import file ID} {--update_stock=1 : Update stock (1/0)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post product batches to WooCommerce API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        set_time_limit(3600);
        
        $size = $this->option('size');
        $rows = $this->option('rows');
        $import_file_id = $this->option('import_file_id');
        $update_stock = boolval($this->option('update_stock'));
        
        $this->output->title('Starting import and loading batches');
        
        $import_file = ImportFile::find($import_file_id);
        
        if ($import_file) {
            $file = storage_path('app/private/imported/excel/' . $import_file->filename);
        } else {
            $file = storage_path('app/test/' . config('importer.excel.import.test.filename.excel'));
        }
        
        $this->comment('Import file: ' . $file);
        $this->comment('-----------------------------------------------------------------');
        $this->newLine();
        
        $collection = (new ProductsImport)->toCollection($file);
        
        $total_rows = $rows ?? count($collection[0]);
        $total_time = 0;
        $from = 0;
        $to = 0;
        $batches = [];
        
        for ($i = 0; $i <= $total_rows; $i = $i + $size) {
            $from = $i;
            $to = $i + $size;
            
            $batches[] = new PostProducts($from, $to, $import_file->id, $update_stock);
            
            $this->line('From: ' . $from . ' --> To: ' . $to);
            $total_time += $total_time;
        }
        
        $batch = Bus::batch($batches)->then(function (Batch $batch) {
            // All jobs completed successfully...
        })->catch(function (Batch $batch, Throwable $e) {
            // First batch job failure detected...
        })->finally(function (Batch $batch) {
            // The batch has finished executing...
            $import_file = ImportFile::where('job_batch_id', $batch->id)->first();
            if ($import_file) {
                $import_file->state = 'inactive';
                $import_file->job_batch_finished_at = now();
                $import_file->save();
            }
        })->name("Toodete import ({$total_rows} tk)")->dispatch();
        
        // Update: ImportFile with batch data
        if ($import_file) {
            $import_file->state = 'active';
            $import_file->job_batch_id = $batch->id;
            $import_file->job_batch_started_at = now();
            $import_file->update_stock = $update_stock;
            $import_file->save();
        }

        $this->output->success('Batches loaded succesfully. Total time spent ' . $total_time . 's.');
        $this->comment('Batch ID: ' . $batch->id);
        $this->newLine();
    }
}
