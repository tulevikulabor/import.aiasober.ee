<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Codexshaper\WooCommerce\Facades\Tag;

class WooCommerceTagsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wc:tags {--page=1 : Page} {--lang=et : Language (et, en, ru, all)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'WooCommerce Tags';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $page = $this->option('page');
        $lang = $this->option('lang');
        
        $tags = Tag::paginate(100, strval($page), ['lang' => $lang]);
        
        $this->info(json_encode($tags, JSON_PRETTY_PRINT));
        $this->comment('Total: ' . count($tags['data']));
    }
}
