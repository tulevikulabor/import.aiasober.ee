<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;
use Automattic\WooCommerce\Client;
use Codexshaper\WooCommerce\Facades\Product;
use Illuminate\Support\Str;
use App\Support\Timer;
use App\Http\WooCommerce\Requests\PostProduct;
use Illuminate\Support\Facades\Cache;
use App\Models\ImportFile;

class PostProductWoocommerceAPICommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wc:post-product {--from=0 : From (Example: 0)} {--to=49 : To (Example: 49)} {--import_file_id= : Import file ID} {--update_stock=1 : Update stock (1/0)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post product to WooCommerce API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        set_time_limit(3600);
        
        $from = $this->option('from');
        $to = $this->option('to');
        $import_file_id = $this->option('import_file_id');
        $update_stock = boolval($this->option('update_stock'));
        
        $this->output->title('Starting import');
        
        $import_file = ImportFile::find($import_file_id);
        
        if ($import_file) {
            $collection = Cache::remember('wc-post-product-excel-' . $import_file->id, 900, function () use ($import_file) {
                $file = storage_path('app/private/imported/excel/' . $import_file->filename);
                $collection = (new ProductsImport)->toCollection($file);
                return $collection;
            });
        } else {
            $collection = Cache::remember('wc-post-product-excel', 1800, function () {
                $file = storage_path('app/test/' . config('importer.excel.import.test.filename.excel'));
                $collection = (new ProductsImport)->toCollection($file);
                return $collection;
            });
        }
        
        $total_time = 0;
        
        for ($i=$from; $i<=$to; $i++) {
            if (isset($collection[0][$i])) {
                $row = $collection[0][$i];
        
                $post = new PostProduct($row, $update_stock);
                $result = $post->send();
                $this->comment(($i+1). ') ' . $result['time'] . 's - ' . $result['product']['sku']);
            
                $total_time += doubleval($result['time']);
            }
        }

        $this->output->success('Import successful. ' . ($to - $from) . ' products imported. Total time spent ' . $total_time . 's.');
    }
}
