<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Codexshaper\WooCommerce\Facades\Product;

class WooCommerceCountCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wc:count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'WooCommerce Product count';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $default_count = Product::count();
        $all_count = $this->getProductsCount('all');
        $et_count = $this->getProductsCount('et');
        $en_count = $this->getProductsCount('en');
        $ru_count = $this->getProductsCount('ru');
        
        $file = storage_path('app/test/' . config('importer.excel.import.test.filename.excel'));
        
        $this->newLine();
        $this->info('API: ' . parse_url(config('woocommerce.store_url'), PHP_URL_HOST));
        $this->info('Import file: ' . $file);
        $this->table(
            ['Lang', 'Count'],
            [
                ['<fg=default>default</>', '<fg=default>' . $default_count . '</>'],
                ['<fg=white>all</>', '<fg=white>' . $all_count . '</>'],
                ['<fg=blue>et</>', '<fg=blue>' . $et_count . '</>'],
                ['<fg=yellow>en</>', '<fg=yellow>' . $en_count . '</>'],
                ['<fg=red>ru</>', '<fg=red>' . $ru_count . '</>'],
            ],
        );
        $this->newLine();
    }
    
    protected function getProductsCount($lang)
    {
        $data = Product::paginate(1, 1, ['lang' => $lang]);
        
        return empty($data['meta']['total_results']) ? null : $data['meta']['total_results'];
    }
}
