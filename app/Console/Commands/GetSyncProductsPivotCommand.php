<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TempProduct;
use DB;

class GetSyncProductsPivotCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:products-pivot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get sync products to pivot array';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::enableQueryLog();
        //$products = TempProduct::where('language', 'et')->limit(10);
        //$childs = $products->childs()->get();
        //echo json_encode($childs, JSON_PRETTY_PRINT);
        $this->info(json_encode((new TempProduct)->exportQuery()->limit(1000)->get(), JSON_PRETTY_PRINT));
        dd(DB::getQueryLog());
    }
}
