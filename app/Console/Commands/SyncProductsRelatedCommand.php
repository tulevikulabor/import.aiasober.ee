<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SyncSession;
use Carbon\Carbon;
use App\Models\TempProduct;
use Illuminate\Support\Str;

set_time_limit(3600);

class SyncProductsRelatedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:products-related';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync products related IDs with SKUs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Update Related SKUs
        
        $products = TempProduct::select('id', 'external_id', 'related_ids')->whereNotNull('related_ids')->get();

        foreach ($products as $product) {
            $related_skus = [];
            $related_ids = json_decode($product->related_ids);

            foreach ($related_ids as $related_id) {
                $pro = TempProduct::select('external_id', 'sku')->where('external_id', $related_id)->first();
                if (isset($pro->sku)) {
                    $related_skus[] = $pro->sku;
                } else {
                    $upsell_skus[] = 'ERROR';
                }
            }

            TempProduct::where('external_id', $product->external_id)->update(['related_skus' => json_encode($related_skus)]);
        }
        
        // Update UpSells SKUs
        
        $products = TempProduct::select('id', 'external_id', 'upsell_ids')->whereNotNull('upsell_ids')->get();
                        
        foreach ($products as $product) {
            $upsell_skus = [];
            $upsell_ids = json_decode($product->upsell_ids);
            
            foreach ($upsell_ids as $upsell_id) {
                $pro = TempProduct::select('external_id', 'sku')->where('external_id', $upsell_id)->first();

                if (isset($pro->sku)) {
                    $upsell_skus[] = $pro->sku;
                } else {
                    $upsell_skus[] = 'ERROR';
                }
            }
                        
            TempProduct::where('external_id', $product->external_id)->update(['upsell_skus' => json_encode($upsell_skus)]);
        }
    }
}
