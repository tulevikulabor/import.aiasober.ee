<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Codexshaper\WooCommerce\Facades\Product;

class WooCommerceProductCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wc:product
                            {id : Product ID (222) or SKU (T20201228211233)}
                            {--filter=id : Filter by "id" or "sku"}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'WooCommerce Product by ID or SKU';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('id');
        $filter = $this->option('filter');
        
        switch ($filter) {
            
            case 'id':
                $product = Product::find($id);
                break;
        
            case 'sku':
                $product = Product::where('sku', $id)->first();
                break;
        }
        
        $this->info(json_encode($product, JSON_PRETTY_PRINT));
    }
}
