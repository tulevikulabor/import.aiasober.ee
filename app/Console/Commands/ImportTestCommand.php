<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\SimpleExcel\SimpleExcelReader;
use \SimpleXLSX;

class ImportTestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:test
                            {file=app/test/test_excel.xlsx : Excel file path to storage}
                            {--parser=simple : Parser "simple" or "spatie"}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Excel import test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file = $this->argument('file');
        $parser = $this->option('parser');
        $path = storage_path($file);
        
        switch ($parser) {
            
            case 'simple':
                $this->getSimpleParser($path);
                break;
                
            case 'spatie':
                $this->getSpatieParser($path);
                break;
        }
    }
    
    protected function getSimpleParser($file)
    {
        if ($xlsx = SimpleXLSX::parse($file)) {
            $this->info(json_encode($xlsx->rows(), JSON_PRETTY_PRINT));
        } else {
            $this->error(SimpleXLSX::parseError());
        }
    }
    
    protected function getSpatieParser($file)
    {
        if ($rows = SimpleExcelReader::create($file)->noHeaderRow()->getRows()) {
            $this->info(json_encode($rows, JSON_PRETTY_PRINT));
        } else {
            $this->error('Error with Spatie SimpleExcelReader');
        }
    }
}
