<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SyncTags;
use App\Models\SyncSession;
use Carbon\Carbon;
use Codexshaper\WooCommerce\Facades\Tag;

//use App\Events\SyncTagsStarted;
//use App\Events\SyncTagsFinished;

class SyncTagsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:tags {--page=1 : Page} {--lang=et : Language (et, en, ru, all)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync tags from WooCommerce API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $page = $this->option('page');
        $lang = $this->option('lang');
        
        $tags = Tag::paginate(100, strval($page), ['lang' => $lang]);
        
        SyncTags::dispatch($tags['data'], $tags['meta'], $lang)->onQueue('tags');
    }
}
