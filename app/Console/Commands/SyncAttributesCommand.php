<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SyncAttributes;
use App\Models\SyncSession;
use Carbon\Carbon;
use Codexshaper\WooCommerce\Facades\Attribute;

//use App\Events\SyncAttributesStarted;
//use App\Events\SyncAttributesFinished;

class SyncAttributesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:attributes {--page=1 : Page} {--lang=et : Language (et, en, ru, all)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync attributes from WooCommerce API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $page = $this->option('page');
        $lang = $this->option('lang');
        
        $attributes = Attribute::paginate(100, strval($page), ['lang' => $lang]);
        
        SyncAttributes::dispatch($attributes['data'], $attributes['meta'], $lang)->onQueue('attributes');
    }
}
