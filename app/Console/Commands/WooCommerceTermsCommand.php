<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Codexshaper\WooCommerce\Facades\Term;

class WooCommerceTermsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wc:terms {attribute_id : Attribute ID (1)} {--page=1 : Page} {--lang=et : Language (et, en, ru, all)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'WooCommerce attribute terms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $attribute_id = $this->argument('attribute_id');
        $page = $this->option('page');
        $lang = $this->option('lang');
        
        $terms = Term::paginate($attribute_id, 100, strval($page), ['lang' => $lang]);

        $this->info(json_encode($terms, JSON_PRETTY_PRINT));
        $this->comment('Total: ' . count($terms['data']));
    }
}
