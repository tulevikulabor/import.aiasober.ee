<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SyncSession;
use Carbon\Carbon;
use Codexshaper\WooCommerce\Facades\Product;
use App\Jobs\SyncProducts;
use App\Events\SyncProductsStarted;
use App\Events\SyncProductsPageChanged;
use App\Events\SyncProductsFinished;

set_time_limit(3600);

class SyncProductsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync products from WooCommerce API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $session = SyncSession::where('finished_at', null)->orderBy('id', 'desc')->first();
        
        // New session
        if (!$session) {
            $session = new SyncSession();
            $session->started_at = Carbon::now();
            $session->page = $session->page + 1;
            $session->save();
            
            SyncProductsStarted::dispatch($session);
            
            $session = SyncSession::where('finished_at', null)->orderBy('id', 'desc')->first();
        }
        
        // Session exists
        if ($session) {
            $page = $session->page;
            $next_page = $page;
                
            while ($next_page) {
                $products = Product::paginate(100, ($page > 0) ? $page : strval(1), ['lang' => 'all']);
                $pages = $products['meta']['total_pages'];
                $total_results = $products['meta']['total_results'];
                $current_page = $products['meta']['current_page'];
                $next_page = $products['meta']['next_page'];
                
                SyncProducts::dispatch($products['data'], $products['meta'])->onQueue('products');
            
                if ($next_page) {
                    $session = (new SyncSession)->find($session->id);
                    $session->page_changed_at = Carbon::now();
                    $session->products = $total_results;
                    $session->pages = $pages;
                    $session->page = $next_page;
                    $session->save();
                    
                    SyncProductsPageChanged::dispatch($session);
                } else {
                    $session = (new SyncSession)->find($session->id);
                    $session->page_changed_at = Carbon::now();
                    $session->finished_at = Carbon::now();
                    $session->save();
                    
                    SyncProductsFinished::dispatch($session);
                }
                
                $session = SyncSession::where('finished_at', null)->orderBy('id', 'desc')->first();
                
                $page = $next_page;
            }
        }
    }
}
