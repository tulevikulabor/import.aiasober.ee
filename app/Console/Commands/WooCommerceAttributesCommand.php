<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Codexshaper\WooCommerce\Facades\Attribute;

class WooCommerceAttributesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wc:attributes {--page=1 : Page} {--lang=et : Language (et, en, ru, all)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'WooCommerce Attributes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $page = $this->option('page');
        $lang = $this->option('lang');
        
        $attributes = Attribute::paginate(100, strval($page), ['lang' => $lang]);
        
        $this->info(json_encode($attributes, JSON_PRETTY_PRINT));
        $this->comment('Total: ' . count($attributes['data']));
    }
}
