<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\ProductsImport;
use App\Support\Timer;
use Corcel\WooCommerce\Model\Product;
use App\Models\TempProduct;

class UpdateProductsStockWoocommerceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wc:update-product-stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update product stock via to WooCommerce DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        set_time_limit(3600);
        
        $this->output->title('Starting import');
        
        $file = storage_path('app/test/' . config('importer.excel.import.test.filename.excel'));
        $collection = (new ProductsImport)->toCollection($file);
        
        $from = 1;
        $to = 5;
        
        $total_time = 0;
                
        for ($i=$from; $i<$to+1; $i++) {
            $row = $collection[0][$i];
            
            $temp_product = TempProduct::where('sku', '')->where('language', 'et')->first();
            $update = Product::find($temp_product->external_id);
            dd($update->sku);
            $this->line($update);
            
            //$this->comment($i. ') ' . $result['time'] . 's - ' . $result['product']['sku']);
            $total_time += doubleval($result['time']);
        }

        $this->output->success('Import successful. ' . ($to - $from) . ' products imported. Total time spent ' . $total_time . 's.');
    }
}
