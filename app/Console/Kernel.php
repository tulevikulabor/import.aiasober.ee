<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly()->withoutOverlapping(60) // 60 min;
        
        // WooCommerce Categories
        
        $schedule->command('sync:categories', ['--lang' => 'et'])->dailyAt('22:15');
        $schedule->command('sync:categories', ['--lang' => 'en'])->dailyAt('22:15');
        $schedule->command('sync:categories', ['--lang' => 'ru'])->dailyAt('22:15');
        
        // WooCommerce Attributes
        
        $schedule->command('sync:attributes', ['--lang' => 'et'])->dailyAt('22:30');
        $schedule->command('sync:attributes', ['--lang' => 'en'])->dailyAt('22:30');
        $schedule->command('sync:attributes', ['--lang' => 'ru'])->dailyAt('22:30');
        
        // WooCommerce Tags
        
        $schedule->command('sync:tags', ['--lang' => 'et'])->dailyAt('22:45');
        $schedule->command('sync:tags', ['--lang' => 'en'])->dailyAt('22:45');
        $schedule->command('sync:tags', ['--lang' => 'ru'])->dailyAt('22:45');
        
        // WooCommerce Products
        
        //$schedule->command('sync:products')->dailyAt('5:00');
        //$schedule->command('sync:products')->dailyAt('12:00');
        //$schedule->command('sync:products')->dailyAt('17:00');
        $schedule->command('sync:products')->dailyAt('23:00');
        
        // WooCommerce Products related
        
        //$schedule->command('sync:products-related')->dailyAt('6:30');
        //$schedule->command('sync:products-related')->dailyAt('12:30');
        //$schedule->command('sync:products-related')->dailyAt('17:30');
        $schedule->command('sync:products-related')->dailyAt('23:30');

        // TODO:
        // PM2 restart
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
