<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\TempAttribute;
use App\Jobs\SyncTerms;
use Codexshaper\WooCommerce\Facades\Term;
use Illuminate\Support\Facades\Artisan;

class SyncAttributes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 300;
    
    protected $attributes;
    protected $meta;
    protected $language;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($attributes, $meta, $language)
    {
        $this->attributes = $attributes;
        $this->meta = $meta;
        $this->language = $language;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Sync Attributes
        
        foreach ($this->attributes as $attribute) {
            $data = [
                'external_id' => empty($attribute->id) ? null : $attribute->id,
                'language' => $this->language,
                'name' => empty($attribute->name) ? null : $attribute->name,
                'slug' => empty($attribute->slug) ? null : $attribute->slug,
                'type' => empty($attribute->type) ? null : $attribute->type,
                'order_by' => empty($attribute->order_by) ? null : $attribute->order_by,
                'has_archives' => empty($attribute->has_archives) ? false : $attribute->has_archives,
                'payload' => empty($attribute) ? null : json_encode($attribute),
            ];
            
            $temp = TempAttribute::where('external_id', $attribute->id)->where('language', $this->language)->first();
            
            if ($temp) {
                TempAttribute::where('external_id', $attribute->id)->where('language', $this->language)->update($data);
            } else {
                TempAttribute::create($data);
            }
        }
        
        // Sync Attribute Terms
        
        foreach ($this->attributes as $attribute) {
            Artisan::call('sync:terms', [
                'attribute_id' => $attribute->id,
                '--page' => 0,
                '--lang' => $this->language,
            ]);
        }
    }
    
    // Send user notification of failure, etc...
    public function failed(Throwable $exception)
    {
        // Send user notification of failure, etc...
    }
}
