<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\TempTerm;

class SyncTerms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 300;
    
    protected $attribute_external_id;
    protected $terms;
    protected $meta;
    protected $language;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($attribute_external_id, $terms, $meta, $language)
    {
        $this->attribute_external_id = $attribute_external_id;
        $this->terms = $terms;
        $this->meta = $meta;
        $this->language = $language;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->terms as $term) {
            $data = [
                'external_id' => empty($term->id) ? null : $term->id,
                'attribute_external_id' => $this->attribute_external_id,
                'language' => $this->language,
                'name' => empty($term->name) ? null : $term->name,
                'slug' => empty($term->slug) ? null : $term->slug,
                'description' => empty($term->description) ? null : $term->description,
                'menu_order' => empty($term->menu_order) ? 0 : $term->menu_order,
                'count' => empty($term->count) ? 0 : $term->count,
                'payload' => empty($term) ? null : json_encode($term),
            ];
            
            $temp = TempTerm::where('external_id', $term->id)->first();
            
            if ($temp) {
                TempTerm::where('external_id', $term->id)->update($data);
            } else {
                TempTerm::create($data);
            }
        }
    }
    
    // Send user notification of failure, etc...
    public function failed(Throwable $exception)
    {
        // Send user notification of failure, etc...
    }
}
