<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\TempCategory;

class SyncCategories implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 300;
    
    protected $categories;
    protected $meta;
    protected $language;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($categories, $meta, $language)
    {
        $this->categories = $categories;
        $this->meta = $meta;
        $this->language = $language;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->categories as $category) {
            $data = [
                'external_id' => empty($category->id) ? null : $category->id,
                'parent_external_id' => empty($category->parent) ? 0 : $category->parent,
                'language' => $this->language,
                'name' => empty($category->name) ? null : $category->name,
                'slug' => empty($category->slug) ? null : $category->slug,
                'description' => empty($category->description) ? null : $category->description,
                'display' => empty($category->display) ? null : $category->display,
                'image' => empty($category->image) ? null : json_encode($category->image),
                'menu_order' => empty($category->menu_order) ? null : $category->menu_order,
                'count' => empty($category->count) ? null : $category->count,
                'payload' => empty($category) ? null : json_encode($category),
            ];
            
            $temp = TempCategory::where('external_id', $category->id)->first();
            
            if ($temp) {
                TempCategory::where('external_id', $category->id)->update($data);
            } else {
                TempCategory::create($data);
            }
        }
    }
    
    // Send user notification of failure, etc...
    public function failed(Throwable $exception)
    {
        // Send user notification of failure, etc...
    }
}
