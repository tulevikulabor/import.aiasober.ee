<?php

namespace App\Jobs;

use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Artisan;
use App\Http\WooCommerce\Requests\PostProduct;

set_time_limit(3600);

class PostProducts implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 300;
    
    /**
     * The maximum number of unhandled exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;
    
    public $from;
    public $to;
    public $import_file_id;
    public $update_stock;
    
    public function __construct($from, $to, $import_file_id = null, $update_stock = null)
    {
        $this->from = $from;
        $this->to = $to;
        $this->import_file_id = $import_file_id;
        $this->update_stock = $update_stock;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() // README: https://www.oulub.com/en-US/Laravel/queues-job-batching, https://ahmedash.com/p/laravel-jobs-batching
    {
        if ($this->batch()->cancelled()) {
            // Determine if the batch has been cancelled...
            
            Log::channel('slack')->error('PostProductsBathes job cancelled ', []);

            return;
        }

        Artisan::call('wc:post-product', [
            '--from' => $this->from,
            '--to' => $this->to,
            '--import_file_id' => $this->import_file_id,
            '--update_stock' => $this->update_stock,
        ]);
    }
}
