<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\TempTag;

class SyncTags implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 300;
    
    protected $tags;
    protected $meta;
    protected $language;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($tags, $meta, $language)
    {
        $this->tags = $tags;
        $this->meta = $meta;
        $this->language = $language;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->tags as $tag) {
            if (!empty($tag->name)) {
                $short_name = array_search($tag->name, config('importer.excel.tags'));
            } else {
                $short_name = null;
            }
            
            $data = [
                'external_id' => empty($tag->id) ? null : $tag->id,
                'language' => $this->language,
                'name' => empty($tag->name) ? null : $tag->name,
                'short_name' => $short_name,
                'slug' => empty($tag->slug) ? null : $tag->slug,
                'description' => empty($tag->description) ? null : $tag->description,
                'count' => empty($tag->count) ? null : $tag->count,
                'payload' => empty($tag) ? null : json_encode($tag),
            ];
            
            $temp = TempTag::where('external_id', $tag->id)->first();
            
            if ($temp) {
                TempTag::where('external_id', $tag->id)->update($data);
            } else {
                TempTag::create($data);
            }
        }
    }
    
    // Send user notification of failure, etc...
    public function failed(Throwable $exception)
    {
        // Send user notification of failure, etc...
    }
}
