<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\TempProduct;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class SyncProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 300;
    
    protected $products;
    protected $meta;
    protected $product_failed;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($products, $meta)
    {
        $this->products = $products;
        $this->meta = $meta;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->products as $product) {
            $this->product_failed = $product;
            
            // Pass broken product
            if (empty($product->sku) || empty($product->lang)) {
                continue;
            }
            
            $images = null;
            if (!empty($product->images)) {
                foreach ($product->images as $image) {
                    $images[] = $image->src;
                }
            }
            
            $latin_name = null;
            if (!empty($product->meta_data)) {
                $latin_name = $this->getProductMeta($product->meta_data, 'latin_name');
            }
            
            $notes = null;
            if (!empty($product->meta_data)) {
                $notes = $this->getProductMeta($product->meta_data, 'markused_ja_lisainfo');
            }
            
            $species_description = null;
            if (!empty($product->meta_data)) {
                $species_description = $this->getProductMeta($product->meta_data, 'liigikirjeldus');
            }
            
            $categories = null;
            if (!empty($product->categories)) {
                foreach ($product->categories as $category) {
                    $categories[] = $category->name;
                }
            }
            $categories = empty($categories) ? [] : array_unique($categories);
            
            $tags = null;
            if (!empty($product->tags)) {
                foreach ($product->tags as $tag) {
                    $tags[] = $tag->name;
                }
            }
            $tags = empty($tags) ? [] : array_unique($tags);
            
            $data = [
                'external_id' => empty($product->id) ? null : $product->id,
                'sku' => empty($product->sku) ? null : $product->sku,
                'type' => empty($product->type) ? null : $product->type,
                'language' => empty($product->lang) ? null : $product->lang,
                'status' => empty($product->status) ? null : $product->status,
                'catalog_visibility' => empty($product->catalog_visibility) ? null : $product->catalog_visibility,
                'latin_name' => empty($latin_name) ? null : $latin_name,
                'name' => empty($product->name) ? null : $product->name,
                'slug' => empty($product->slug) ? null : $product->slug,
                'description' => strip_tags($product->description),
                'short_description' => strip_tags($product->short_description),
                'price' => empty($product->price) ? null : $product->price,
                'regular_price' => empty($product->regular_price) ? null : $product->regular_price,
                'sale_price' => empty($product->sale_price) ? null : $product->sale_price,
                'stock_quantity' => empty($product->stock_quantity) ? null : $product->stock_quantity,
                'stock_status' => empty($product->stock_status) ? null : $product->stock_status,
                'permalink' => empty($product->permalink) ? null : $product->permalink,
                'categories' => empty($categories) ? null : json_encode($categories),
                'related_ids' => empty($product->related_ids) ? null : json_encode($product->related_ids),
                'upsell_ids' => empty($product->upsell_ids) ? null : json_encode($product->upsell_ids),
                'tags' => empty($tags) ? null : json_encode($tags),
                'images' => empty($images) ? null : json_encode($images),
                'attribute_1' => empty($product->attributes) ? null : $this->getAttributeName($product->attributes, 1),
                'attribute_1_value' => empty($product->attributes) ? null : $this->getAttributeValue($product->attributes, 1),
                'attribute_2' => empty($product->attributes) ? null : $this->getAttributeName($product->attributes, 2),
                'attribute_2_value' => empty($product->attributes) ? null : $this->getAttributeValue($product->attributes, 2),
                'attribute_3' => empty($product->attributes) ? null : $this->getAttributeName($product->attributes, 3),
                'attribute_3_value' => empty($product->attributes) ? null : $this->getAttributeValue($product->attributes, 3),
                'attribute_4' => empty($product->attributes) ? null : $this->getAttributeName($product->attributes, 4),
                'attribute_4_value' => empty($product->attributes) ? null : $this->getAttributeValue($product->attributes, 4),
                'notes' => empty($notes) ? null : $notes,
                'species_description' => empty($species_description) ? null : $species_description,
                'date_created' => empty($product->date_created) ? null : $product->date_created,
                'date_modified' => empty($product->date_modified) ? null : $product->date_modified,
                'payload' => empty($product) ? null : json_encode($product),
            ];
            
            // If translation exists
            
            $temp = TempProduct::where('sku', $product->sku)->where('language', $product->lang)->first();
        
            if ($temp) {
                TempProduct::where('sku', $product->sku)->where('language', $product->lang)->update($data);
            } else {
                TempProduct::create($data);
            }
        }
    }
    
    protected function getProductMeta($meta_data, $key)
    {
        $return = null;
        
        foreach ($meta_data as $meta) {
            if ($meta->key == $key) {
                $return = $meta->value;
                break;
            }
        }
        return $return;
    }
    
    protected function getAttributeName($attributes, $position)
    {
        return isset($attributes[$position-1]) ? $attributes[$position-1]->name : null;
    }
    
    protected function getAttributeValue($attributes, $position)
    {
        return isset($attributes[$position-1]) ? (isset($attributes[$position-1]->options[0]) ? $attributes[$position-1]->options[0] : null) : null;
    }
    
    // Send user notification of failure, etc...
    public function failed() // Throwable $exception
    {
        Log::channel('slack')->error('SyncProducts job error - Product language not exists ', collect($this->product_failed)->toArray());
    }
}
