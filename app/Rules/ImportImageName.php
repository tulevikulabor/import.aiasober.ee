<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class ImportImageName implements Rule
{
    protected $request;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $original_filename = $this->request->{$attribute}->getClientOriginalName();
        
        if (preg_match('/^\d{5,7}(\.\d+)?\.\D{3,4}$/i', $original_filename)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Image name format is incorrect (For example "123456.1.jpg" or "123456.jpg").';
    }
}
