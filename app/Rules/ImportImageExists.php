<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\ImportImage;
use Illuminate\Http\Request;

class ImportImageExists implements Rule
{
    protected $request;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $original_filename = $this->request->{$attribute}->getClientOriginalName();
        $import_image = ImportImage::where('filename', $original_filename)->first();
        
        return ($import_image) ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Filename already exists in database.';
    }
}
