<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class EmailHost implements Rule
{
    protected $host;
    protected $value;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->host = email($value)->host;
        $this->value = email($value)->email;

        if ($this->host && checkdnsrr($this->host, "MX")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans("The :attribute host \":host\" does not exist.", [
            'host' => $this->host,
            'value' => $this->value,
        ]);
    }
}
