<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobBatch extends Model
{
    use HasFactory;
    
    protected $guarded = [];
    
    public function getActiveSession()
    {
        return $this->where('cancelled_at', null)->where('finished_at', null)->orderBy('created_at', 'asc')->first();
    }
    
    public function getLastSession()
    {
        return $this->orderBy('id', 'desc')->first();
    }
}
