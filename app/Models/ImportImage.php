<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImportImage extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    
    public function getImageByFilename($filename)
    {
        return $this->where('filename', $filename)->orderBy('id', 'desc')->first();
    }
    
    public function getImagesBySku($sku)
    {
        return $this->where('sku', $sku)->orderBy('order', 'asc')->get();
    }
}
