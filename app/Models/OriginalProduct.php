<?php

namespace App\Models;

use App\Models\TempProduct;

class OriginalProduct extends TempProduct
{
    protected $table = 'original_products';
}
