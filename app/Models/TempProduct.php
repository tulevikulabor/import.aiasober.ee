<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class TempProduct extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'attribute_1_value' => 'string',
        'attribute_2_value' => 'string',
        'attribute_3_value' => 'string',
        'attribute_4_value' => 'string',
    ];

    public function getTagsAttribute($value)
    {
        if ($value) {
            return implode(', ', json_decode($value, true));
        } else {
            return null;
        }
    }

    public function getCategoriesAttribute($value)
    {
        if ($value) {
            return implode('; ', json_decode($value, true));
        } else {
            return null;
        }
    }

    public function getRelatedSkusAttribute($value)
    {
        if ($value) {
            return implode(', ', json_decode($value, true));
        } else {
            return null;
        }
    }

    public function getUpsellSkusAttribute($value)
    {
        if ($value) {
            return implode(', ', json_decode($value, true));
        } else {
            return null;
        }
    }

    public function getImagesAttribute($value)
    {
        $array = [];

        if ($value) {
            $images = json_decode($value);

            foreach ($images as $image) {
                $array[] = basename($image);
            }

            return implode(', ', $array);
        } else {
            return null;
        }
    }

    public function getImagesFullAttribute($value)
    {
        if ($value) {
            return implode(', ', json_decode($value, true));
        } else {
            return null;
        }
    }

    public function products()
    {
        //return $this->hasMany(TempProduct::class, 'sku', 'sku');
        //return $this->belongsToMany(TempProduct::class, 'user_roles', 'user_id', 'role_id');
    }

    /*
    |--------------------------------------------------------------------------
    | Export: Raw select
    |--------------------------------------------------------------------------
    */

    public function getExporViewRaw($limit = 10, $offset = 0)
    {
        return DB::select("
            select
                sku,
                max(case when (language = 'et') then name else NULL end) as `name_et`,
                max(case when (language = 'en') then name else NULL end) as `name_en`,
                max(case when (language = 'ru') then name else NULL end) as `name_ru`,
                max(case when (language = 'et') then latin_name else NULL end) as `latin_name`
            from
                temp_products
            where
                language IS NOT NULL
            group by
                sku
            order by
                sku
            limit :limit
            offset :offset
        ", [
            'limit' => $limit,
            'offset' => $offset,
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | Export: Dynamic select
    |--------------------------------------------------------------------------
    */

    public function scopeExportQuery($query)
    {
        return $query->selectExport()->whereNotNull('language')->whereNotNull('status')->groupBy('sku')->orderBy('sku');
    }

    public function scopeSelectExport($query)
    {
        return $query->selectTranslation('sku', 'et')
            ->selectTranslation('latin_name', 'et')
            ->selectTranslation('name', 'et', true)
            ->selectTranslation('name', 'en', true)
            ->selectTranslation('name', 'ru', true)
            ->selectTranslation('status', 'et')
            ->selectTranslation('categories', 'et')
            ->selectTranslation('upsell_skus', 'et')
            ->selectTranslation('tags', 'et')
            ->selectTranslation('stock_quantity', 'et')
            ->selectTranslation('price', 'et')
            //->selectTranslation('regular_price', 'et')
            ->selectTranslation('attribute_1', 'et')
            ->selectTranslation('attribute_1_value', 'et')
            ->selectTranslation('attribute_2', 'et')
            ->selectTranslation('attribute_2_value', 'et')
            ->selectTranslation('attribute_3', 'et')
            ->selectTranslation('attribute_3_value', 'et')
            ->selectTranslation('attribute_4', 'et')
            ->selectTranslation('attribute_4_value', 'et')
            ->selectTranslation('species_description', 'et')
            ->selectTranslation('notes', 'et', true)
            ->selectTranslation('notes', 'en', true)
            ->selectTranslation('notes', 'ru', true)
            ->selectTranslation('description', 'et')
            ->selectTranslation('images', 'et')
            ->selectTranslation('images', 'et', false, 'images_full')
            // ->selectTranslation('id', 'et')
            // ->selectTranslation('external_id', 'et')
        ;
    }

    public function scopeSelectTranslation($query, $field, $language, $add_language_to_field = false, $field_as = null, $language_field = 'language')
    {
        $field_as = $field_as ?? $field;

        if ($add_language_to_field) {
            $field_as = "{$field_as}_{$language}";
        }

        return $query->selectRaw("max(case when (`{$language_field}` = '{$language}') then `{$field}` else NULL end) as `{$field_as}`");
    }
}
