<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SyncSession extends Model
{
    use HasFactory;
    
    protected $guarded = [];
    
    public function getActiveSession()
    {
        return $this->where('finished_at', null)->orderBy('id', 'desc')->first();
    }
    
    public function getLastSession()
    {
        return $this->orderBy('id', 'desc')->first();
    }
}
