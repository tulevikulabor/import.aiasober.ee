<?php

namespace App\Exports;

use App\Models\TempProduct;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ProductsExport implements
    FromCollection,
    WithHeadings,
    ShouldAutoSize,
    WithColumnWidths,
    WithColumnFormatting,
    WithStyles,
    WithEvents
{
    use Exportable;

    public function headings(): array
    {
        return config('importer.excel.headings');
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]], // Style the first row as bold text.
            'A' => ['font' => ['bold' => true]], // Styling an entire column.
            'B' => ['font' => ['bold' => true]], // Styling an entire column.
            'J' => ['font' => ['bold' => true]], // Styling an entire column.
            'K' => ['font' => ['bold' => true]], // Styling an entire column.
        ];
    }

    public function columnWidths(): array
    {
        return [
            'B' => 35, // Nimetus (LAT)
            'C' => 35, // Nimetus (EST)
            'D' => 35, // Nimetus (ENG)
            'E' => 35, // Nimetus (RUS)
            'G' => 35, // Kategooriad
            'H' => 30, // Seotud tootekoodid
            'I' => 25, // Tingmärgid
            'M' => 20, // Väärtus-1
            'O' => 20, // Väärtus-2
            'Q' => 20, // Väärtus-3
            'S' => 20, // Väärtus-4
            'T' => 25, // Liigikirjeldus
            'U' => 70, // Märkused ja lisainfo (EST)
            'V' => 70, // Märkused ja lisainfo (ENG)
            'W' => 70, // Märkused ja lisainfo (RUS)
            'X' => 130, // Kirjeldus
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT, // Tootekood
            'H' => NumberFormat::FORMAT_TEXT, // Seotud tootekoodid
            'M' => NumberFormat::FORMAT_TEXT, // Väärtus-1
            'O' => NumberFormat::FORMAT_TEXT, // Väärtus-2
            'Q' => NumberFormat::FORMAT_GENERAL, // Väärtus-3
            'S' => NumberFormat::FORMAT_TEXT, // Väärtus-4
        ];
    }

    // https://laravel-news.com/five-hidden-features-of-the-laravel-excel-package

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->freezePane('C2');
                $event->sheet->formatColumn('Q', '@');
            //$event->sheet->formatColumn('Q', '00.0000');
            },
        ];
    }

    public function prepareRows($rows): array
    {
        $statuses = config('importer.excel.status');
        $tags = config('importer.excel.tags');

        return array_map(function ($temp_product) use ($statuses, $tags) {
            $temp_product->status = $this->prepareColumnStatus($temp_product, $statuses);
            $temp_product->tags = $this->prepareColumnTags($temp_product, $tags);
            $temp_product->attribute_1_value = $this->prepareColumnAttributeValue($temp_product->attribute_1_value);
            $temp_product->attribute_2_value = $this->prepareColumnAttributeValue($temp_product->attribute_2_value);
            $temp_product->attribute_3_value = $this->prepareColumnAttributeValue($temp_product->attribute_3_value);
            $temp_product->attribute_4_value = $this->prepareColumnAttributeValue($temp_product->attribute_4_value);

            return $temp_product;
        }, $rows);
    }

    protected function prepareColumnStatus($temp_product, $statuses)
    {
        return $statuses[$temp_product->status];
    }

    protected function prepareColumnTags($temp_product, $tags)
    {
        $array = [];
        foreach (explode(',', $temp_product->tags) as $temp_product_tag) {
            if (trim($temp_product_tag)) {
                $array[] = array_search(trim($temp_product_tag), $tags);
            }
        }

        return json_encode($array);
    }

    protected function prepareColumnAttributeValue($temp_product_attribute_value)
    {
        if (preg_match('/^\d{2}.\d{4}/', $temp_product_attribute_value)) {
            return str_replace(',', '.', strval($temp_product_attribute_value).' ');
        } else {
            return $temp_product_attribute_value;
        }
    }

    public function collection()
    {
        return (new TempProduct)->exportQuery()->get();
    }

    /* public function sheet(Worksheet $sheet){
        $sheet->setFreeze('A2');
    }

    $cells->setValignment('center'); */
}
