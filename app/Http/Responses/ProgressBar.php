<?php

namespace App\Http\Responses;

class ProgressBar
{
    public $min = 0;
    public $max = 0;
    
    public $value = 0;
    
    public $started_at;
    public $progress_at;
    public $finished_at;
    
    public $finished = false;
    
    public $progress_exact = 0;
    public $progress = 0;
    
    public $data = null;
    
    private $error = false;
    
    public function __construct()
    {
    }
        
    public function init()
    {
        try {
            if ($this->max > 0) {
                $this->progress_exact = ($this->value / $this->max) * 100;
                $this->progress = floor($this->progress_exact);
                $this->finished = ($this->finished) ? true : (($this->max == $this->value) ? true : false);
            } else {
                $this->error = true;
            }
        } catch (Exception $e) {
            $this->error = true;
        }
    }
    
    public function toArray()
    {
        $this->init();
        return $this;
    }
    
    public function response()
    {
        if ($this->error) {
            return response()->json(['error' => true], 500);
        } else {
            return response()->json($this->toArray());
        }
    }
}
