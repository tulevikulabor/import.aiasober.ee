<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ImportImage;

class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    
    public function index()
    {
        $images = ImportImage::withTrashed(false)->orderBy('id', 'desc')->paginate(30);
        
        return view('image.index')->with('images', $images);
    }
}
