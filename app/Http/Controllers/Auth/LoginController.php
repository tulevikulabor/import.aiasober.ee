<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RedirectsUsers;
use App\Providers\RouteServiceProvider;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, RedirectsUsers {
          AuthenticatesUsers::redirectPath insteadof RedirectsUsers;
          AuthenticatesUsers::guard insteadof RedirectsUsers;
     }

    //use RedirectsUsers, AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectTo()
    {
        /* if ($this->request->has('previous')) {
        $this->redirectTo = $this->request->get('previous');
        }

        return $this->redirectTo ?? '/home'; */

        if (request()->session()->has('last_visited_item')) {
            //return route('items', ['id' => request()->session()->get('last_visited_item_id')]);
            return request()->session()->get('last_visited_item')->url;
        }
        return route($this->redirectTo, ['locale' => app()->getLocale()]);
    }
}
