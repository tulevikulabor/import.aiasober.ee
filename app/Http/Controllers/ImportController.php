<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Artisan;
use App\Http\Responses\ProgressBar;
use App\Imports\ProductsImport;
use App\Models\JobBatch;
use App\Models\ImportFile;

class ImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    
    /*
    |--------------------------------------------------------------------------
    | Import batches and progress
    |--------------------------------------------------------------------------
    */
    
    public function index(JobBatch $job_batch)
    {
        $pending_import_files = ImportFile::withTrashed(false)->whereIn('state', ['pending', 'active'])->orderBy('id', 'desc')->paginate(5, ['*'], 'pending_page');
        $job_batches = $job_batch->orderBy('id', 'desc')->paginate(5, ['*'], "job_page");
        $inactive_import_files = ImportFile::withTrashed()->where('state', 'inactive')->orderBy('updated_at', 'desc')->paginate(5, ['*'], 'inactive_page');
                
        return view('import.index')->with([
            'job_batches' => $job_batches,
            'pending_import_files' => $pending_import_files,
            'inactive_import_files' => $inactive_import_files,
        ]);
    }
    
    public function getActiveSession(JobBatch $job_batch)
    {
        return response()->json($job_batch->getActiveSession());
    }
    
    public function getProgress(JobBatch $job_batch)
    {
        $data = $job_batch->getActiveSession();
        $data = ($data) ? $data : $job_batch->getLastSession();
                
        $progressBar = new ProgressBar();
        $progressBar->max = $data->total_jobs;
        $progressBar->value = $data->total_jobs - $data->pending_jobs + $data->failed_jobs;
        $progressBar->started_at = $data->created_at;
        $progressBar->progress_at = null;
        $progressBar->finished_at = $data->finished_at;
        
        return $progressBar->response();
    }
    
    public function reload()
    {
        return back();
    }
    
    public function downloadExcel(ImportFile $import_file)
    {
        return response()->download(storage_path('app/private/imported/excel/' . $import_file->filename));
    }
    
    /*
    |--------------------------------------------------------------------------
    | Import batches
    |--------------------------------------------------------------------------
    */
   
    public function runImport(ImportFile $import_file, Request $request)
    {
        if ($import_file && $import_file->state == 'pending' && !$import_file->job_batch_id) {
            $update_stock = boolval($request->update_stock);
                        
            Artisan::call('wc:post-products-batches', [
                '--import_file_id' => $import_file->id,
                '--update_stock' => $update_stock,
            ]);
        }
        
        return back()->withSuccess(__('Import is now running!'));
    }
    
    public function cancelImport(ImportFile $import_file)
    {
        if (!$import_file->job_batch_id) {
            return back()->withError(__('Cancellation failed! Import batch job (:import_file_id) does not exist!', ['import_file_id' => $import_file->id]));
        }
        
        $batch = Bus::findBatch($import_file->job_batch_id);
        
        if ($batch) {
            if ($batch->cancelled()) {
                $import_file->state = 'inactive';
                $import_file->save();
                
                return back()->withError(__('Import batch job already cancelled!'));
            } else {
                $batch->cancel();
                $import_file->state = 'inactive';
                $import_file->save();
                
                return back()->withSuccess(__('Import batch job cancelled!'));
            }
        } else {
            return back()->withError(__('Cancellation failed! Import batch job (:import_file_id) does not exist!', ['import_file_id' => $import_file->id]));
        }
    }
    
    public function trashImport(ImportFile $import_file)
    {
        if ($import_file->state !== 'inactive') {
            $import_file->state = 'inactive';
            $import_file->save();
            $import_file->delete();
        }
        
        return back()->withSuccess(__('Import ":file" is now inactive!', ['file' => $import_file->original_filename]));
    }
}
