<?php

// API: https://woocommerce.github.io/woocommerce-rest-api-docs/?php#retrieve-a-product

namespace App\Http\Controllers\WooCommerce\Corcel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Corcel\WooCommerce\Model\Product;
use App\Support\Timer;
use Str;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        // $this->middleware('superuser');
    }
    
    public function database(Request $request)
    {
        Timer::start('products');
        
        $products = Product::paginate(15);
        
        $page = $request->input('page');
        
        $languages = []; //array_remove_value(config('importer.language.list'), config('importer.language.default'));
        $translations = [];
                            
        Timer::stop('products');
        $timer = Timer::get('products');
        $time = Str::beforeLast($timer['time'], '.');
        
        return view('woocommerce.product.table2')->with([
            'products' => $products,
            'meta' => $products['meta'],
            'translations' => $translations,
            'languages' => $languages,
            'time' => $time,
        ]);
    }
}
