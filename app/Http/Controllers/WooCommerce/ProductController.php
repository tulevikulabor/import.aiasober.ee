<?php

// API: https://woocommerce.github.io/woocommerce-rest-api-docs/?php#retrieve-a-product

namespace App\Http\Controllers\WooCommerce;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Codexshaper\WooCommerce\Facades\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Support\Timer;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
        // $this->middleware('superuser');
    }
    
    public function downloadTestExcel()
    {
        return Storage::download('test/test_excel.xlsx');
    }
    
    public function table(Request $request)
    {
        Timer::start('products');
        
        $page = $request->input('page');
        
        $products = Product::orderBy('sku', 'asc')->paginate(15, ($page > 0) ? $page : strval(1), ['lang' => 'all']);
        
        //$products = Product::all(['per_page' => 100, 'lang' => 'all']);
        
        Timer::stop('products');
        $timer = Timer::get('products');
        $time = Str::beforeLast($timer['time'], '.');
             
        dd($time, $products);
        
        $translations = [];
        
        foreach ($products['data'] as $product) {
            foreach ($product->translations as $language => $product_id) {
                //$translations[$product->id][$language] = Product::find($product_id);
            }
        }
        
        $languages = array_remove_value(config('importer.language.list'), config('importer.language.default'));
                    
        Timer::stop('products');
        $timer = Timer::get('products');
        $time = Str::beforeLast($timer['time'], '.');
                
        return view('woocommerce.product.table')->with([
            'products' => $products['data'],
            'meta' => $products['meta'],
            'translations' => $translations,
            'languages' => $languages,
            'time' => $time,
        ]);
    }
    
    public function index(Request $request)
    {
        $page = $request->input('page');
        
        if ($page) {
            $products = Product::paginate(5, ($page > 0) ? $page : strval(1));
        } else {
            $products = Product::all();
        }
        
        return $products;
    }
    
    public function show($product_id)
    {
        $product = Product::find($product_id);
        
        return $product;
    }
    
    public function filterBySKU($sku)
    {
        $product = Product::where('sku', $sku)->first();
        
        return $product;
    }
    
    public function count()
    {
        $count = Product::count();
        
        return $count;
    }
    
    public function create()
    {
        $time = time();
        $data = [
            'name' => 'Simple Product - ' . date('Y-m-d H:m:s', $time),
            'type' => 'simple',
            'regular_price' => '10.00',
            'description' => 'Simple product full description.',
            'short_description' => 'Simple product short description.',
            'sku' => 'T' . date('YmdHms', $time),
            'categories' => [
                [
                    'id' => 1
                ],
                [
                    'id' => 3
                ],
                [
                    'id' => 5
                ]
            ],
            'images' => [
                [
                    'src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg'
                ],
                [
                    'src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_back.jpg'
                ]
            ],
        ];
        
        $product = Product::create($data);
        
        return $product;
    }
    
    public function update($product_id)
    {
        $data = [
            'regular_price' => '50',
            'sale_price'    => '25',
            'images' => [
                [
                    // 'src' => route('woocommerce.products.gravatar', ['hash' => time()]),
                    'src' => 'https://wp-api.orumets.ee/woocommerce/products/gravatar/' . time().rand(1, 10) . '.png',
                ],
                [
                    // 'src' => route('woocommerce.products.gravatar', ['hash' => time()]),
                    'src' => 'https://wp-api.orumets.ee/woocommerce/products/gravatar/' . time().rand(1, 10) . '.png',
                ],
                [
                    // 'src' => route('woocommerce.products.gravatar', ['hash' => time()]),
                    'src' => 'https://wp-api.orumets.ee/woocommerce/products/gravatar/' . time().rand(1, 10) . '.png',
                ],
            ],
        ];
        
        $product = Product::update($product_id, $data);
        
        return $product;
    }
    
    public function gravatar($hash)
    {
        $url = 'https://gravatar.com/avatar/' . $hash . '?s=400&d=robohash&r=x';
        $image = file_get_contents($url);
        
        $numberToContentTypeMap = [
            '1' => 'image/gif',
            '2' => 'image/jpeg',
            '3' => 'image/png',
            // '6' => 'image/bmp',
            // '17' => 'image/ico'
        ];

        $contentType = $numberToContentTypeMap[exif_imagetype($url)] ?? null;
        
        if ($contentType === null) {
            throw new Exception('Unable to determine content type of file.');
        }
        
        $extension = Str::afterLast($contentType, '/');
                
        return response($image)
            ->header('Content-type', $contentType)
            ->header('Content-disposition', 'inline; filename="' . $hash . '.' . $extension . '"');
    }
}
