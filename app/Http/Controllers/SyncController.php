<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SyncSession;
use App\Http\Responses\ProgressBar;

class SyncController extends Controller
{
    public function getActiveSession(SyncSession $sync_session)
    {
        return response()->json($sync_session->getActiveSession());
    }
    
    public function getProgress(SyncSession $sync_session)
    {
        $data = $sync_session->getLastSession();
        
        $progressBar = new ProgressBar();
        $progressBar->max = $data->pages;
        $progressBar->value = $data->page;
        $progressBar->started_at = $data->started_at;
        $progressBar->progress_at = $data->page_changed_at;
        $progressBar->finished_at = $data->finished_at;
        
        return $progressBar->response();
    }
}
