<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Rules\ImportImageName;
use App\Rules\ImportImageExists;
use App\Models\ImportImage;
use App\Models\ImportFile;

class UploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    
    public function uploadFile(Request $request)
    {
        $this->validate($request, [
            'file' => [
                'required',
                'mimetypes:' . implode(',', \Arr::collapse(config('importer.mime'))),
                'max:50000',
            ],
        ]);
        
        $mimetype = $request->file->getMimetype();
        
        if (in_array($mimetype, config('importer.mime.excel'))) {
            $this->uploadExcel($request);
        } elseif (in_array($mimetype, config('importer.mime.image'))) {
            $this->uploadImage($request);
        } else {
            return abort(400);
        }
    }
    
    public function uploadExcel(Request $request)
    {
        $this->validate($request, [
            'file' => [
                'required',
                'mimetypes:' . implode(',', config('importer.mime.excel')),
                'max:50000',
            ],
        ]);
        
        $extension = $request->file->extension();
        $original_filename = $request->file->getClientOriginalName();
        $original_filename_without_extension = str_replace('.'.$extension, '', $original_filename);
        
        $import_file = ImportFile::create();
        
        $filename = date('Y-m-d') . '__' . $import_file->id . '__' . Str::slug($original_filename_without_extension, '-') . '.' . $extension;
            
        $path = $request->file->storeAs('private/imported/excel', $filename);
        
        ImportFile::whereId($import_file->id)->update([
            'filename' => $filename,
            'original_filename' => $original_filename,
            'user_id' => auth()->user()->id,
        ]);
    }
    
    public function uploadImage(Request $request)
    {
        $this->validate($request, [
            'file' => [
                'required',
                'mimetypes:' . implode(',', config('importer.mime.image')),
                'max:50000',
                new ImportImageName($request),
                new ImportImageExists($request),
            ],
        ]);
        
        $extension = $request->file->extension();
        $original_filename = $request->file->getClientOriginalName();
        $original_filename_without_extension = str_replace('.'.$extension, '', $original_filename);
        
        if (preg_match('/^\d{5,7}(\.\d+)?\.\D{3,4}$/i', $original_filename)) {
            $formatted_filename_without_extension = $original_filename_without_extension;
            $filename = $original_filename;
        } else {
            $formatted_filename_without_extension = Str::slug($original_filename_without_extension, '-');
            $filename = $formatted_filename_without_extension . '.' . $extension;
        }
        
        $img = Image::make($request->file);
        $img->resize(config('importer.image.size.width'), config('importer.image.size.height'), function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $path = 'app/private/imported/images/' . date('Y-m-d');
        if (!File::exists(storage_path($path))) {
            File::makeDirectory(storage_path($path), 0777, true, true);
        }
        $img->save(storage_path($path.'/'.$filename));
                
        if (!ImportImage::where('path', 'storage/'.$path)->where('filename', $filename)->first()) {
            preg_match('/^\d{5,7}.(\d+)?\.\D{3,4}$/i', $filename, $matches, PREG_UNMATCHED_AS_NULL);

            ImportImage::create([
                'sku' => Str::before($filename, '.'),
                'path' => 'storage/'.$path,
                'filename' => $filename,
                'order' => empty($matches[1]) ? 0 : (($matches[1] >= 0) ? $matches[1] : 0),
            ]);
        }
    }
}
