<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TempProduct;
use App\Exports\ProductsExport;
use App\Models\SyncSession;

class ExportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['downloadExcelByHash', 'downloadPageByHash']]);
        $this->middleware('admin', ['except' => ['downloadExcelByHash', 'downloadPageByHash']]);
    }
    
    public function index(SyncSession $sync_session)
    {
        $sync_sessions = $sync_session->orderBy('id', 'desc')->paginate(10);
                
        return view('export.index')->with(['sync_sessions' => $sync_sessions]);
    }
    
    public function downloadCsv()
    {
        return (new ProductsExport)->download(config('importer.excel.export.filename') . '.csv');
    }
    
    public function downloadExcel()
    {
        return (new ProductsExport)->download(config('importer.excel.export.filename') . '.xlsx');
    }
    
    public function downloadExcelByHash()
    {
        return (new ProductsExport)->download(config('importer.excel.export.filename') . '.xlsx');
    }
    
    public function downloadPageByHash()
    {
        return view('export.download_excel');
    }
    
    public function products()
    {
        return view('export.products');
    }
}
