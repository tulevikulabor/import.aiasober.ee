<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use Validator;

class ImporterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    
    public function reloadFile()
    {
        return back();
        
        $url = config('importer.excel.url');
        
        $response = Http::get($url);
        if ($response->successful()) {
            $body = $response->body();
            Storage::put('private/imported/' . config('importer.excel.name'), $body);
            
            return redirect()->route('home')->withSuccess(__('Excel file successfully reloaded from URL!'));
        } else {
            return redirect()->route('index')->withError(__('Cannot import Excel file from URL!'));
        }
    }
}
