<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Spatie\SimpleExcel\SimpleExcelReader;
use \SimpleXLSX;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $path = 'private/imported/' . config('importer.excel.name');
        
        $time = Storage::lastModified($path);
        
        $rows = SimpleExcelReader::create(storage_path('app/' . $path))->noHeaderRow()->getRows()->toArray();
        
        
        echo "<pre>";
        echo json_encode($this->getFormattedArray($rows), JSON_PRETTY_PRINT);
        echo "</pre>";
        
        echo "<pre>";
        echo json_encode($rows, JSON_PRETTY_PRINT);
        echo "</pre>";
        
        return view('home', [
            'time' => Carbon::createFromTimestamp($time)->format('d.m.Y H:i:s'),
        ]);
    }
    
    private function getFormattedArray($rows)
    {
        $array = [];
        
        $cols = $rows[0];
        
        for ($c = 0; $c < count($cols); $c++) {
            $array = [$cols[$c]];
        }
        
        
        return $array;
    }
    
    private function getFormattedArray__($rows)
    {
        $array = [];
        
        for ($i = 0; $i < count($rows); $i++) {
            
            for ($j = 0; $j < count($rows[$i]); $j++) {
                //$array[$i][$j] = $rows[$j];
                if($j !== 4){
                    for($k = 0; $k < 2; $k ++) {
                        if($k == 0) $array[$j][$i]['client'] = $rows[$i][$j];
                        if($k == 1) $array[$j+1][$i]['time'] = $rows[$i][$j];
                    }
                }
            }
        }
        
        return $array;
    }
}
