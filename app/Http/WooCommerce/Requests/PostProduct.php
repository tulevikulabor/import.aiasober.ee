<?php

namespace App\Http\WooCommerce\Requests;

use Codexshaper\WooCommerce\Facades\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\Http\WooCommerce\Requests\Traits\TimerTrait;
use App\Models\TempCategory;
use App\Models\TempTag;
use App\Models\TempProduct;
use App\Models\ImportImage;

//use App\Http\WooCommerce\Requests\Exception;

class PostProduct
{
    use TimerTrait;
    
    public $data;
    public $payload;
    public $product;
    
    public $langs;
    public $lang;
    public $translation_langs;
    
    public $update_stock;
    
    private $product_limit;
    
    protected $timerName = 'post_product';
    
    public function __construct(Collection $data, $update_stock = true)
    {
        $this->data = $data;
        $this->update_stock = $update_stock;
        
        if (empty($this->data['tootekood'])) {
            Log::channel('slack')->error('PostProduct error - $data["tootekood"] missing', collect($this->data)->toArray());
            //throw new \Exception('PostProduct error - $data["sku"] missing');
        }
        
        $this->langs = config('importer.language.list');
        $this->lang = config('importer.language.default');
        $this->translation_langs = array_remove_value(config('importer.language.list'), config('importer.language.default'));
        
        $this->product_limit = count($this->langs);

        $this->timerStart();
        
        $this->product = $this->ifProductExists($this->data['tootekood'], $this->lang);
    }
    
    protected function getExternalImagesPayload($data)
    {
        $images_string = explode(",", $data['piltide_lingid']);
        $images = [];
        $position = 0;
        foreach ($images_string as $image) {
            if (check_remote_file(trim($image))) {
                $images[] = [
                    'alt' => $data['nimetus_est'],
                    'name' => basename(trim($image)),
                    'src' => trim($image),
                    'position' => $position,
                ];
                $position++;
            } else {
                Log::channel('slack')->error('PostProduct error - Product (SKU: ' . $data['tootekood'] . ') image URL not exists', collect($images_string)->toArray());
            }
        }
        return $images;
    }
    
    protected function getImagesPayload($data)
    {
        $images_string = explode(",", $data['pildid']);
        $images = [];
        $position = 0;
        foreach ($images_string as $image) {
            $import_image = ImportImage::where('filename', trim($image))->first();
            if ($import_image) {
                $image_url = asset('imported_images/' . basename($import_image->path) . '/' . $import_image->filename);
                if (check_remote_file(trim($image_url))) {
                    $images[] = [
                        'alt' => $data['nimetus_est'],
                        'name' => $import_image->filename,
                        'src' => trim($image_url),
                        'position' => $position,
                    ];
                    $position++;
                } else {
                    Log::channel('slack')->error('PostProduct error - Product (SKU: ' . $data['tootekood'] . ') image URL not exists', collect($images_string)->toArray());
                }
            }
        }
        return $images;
    }
    
    protected function getUpsellIds($data)
    {
        $upsell_skus = explode(",", $data['seotud_tootekoodid']);
        $upsell_ids = [];
        foreach ($upsell_skus as $upsell_sku) {
            $temp = TempProduct::select('external_id')->where('sku', trim($upsell_sku))->where('language', $this->lang)->first();
            if ($temp) {
                $upsell_ids[] = intval($temp->external_id);
            }
        }
        
        return $upsell_ids;
    }
    
    protected function getCategoriesPayload($data)
    {
        $categories_string = explode(";", $data['kategooriad']);
        
        $categories = [];
        foreach ($categories_string as $category) {
            $temp = TempCategory::select('external_id')->where('name', trim($category))->where('language', $this->lang)->first();
            if ($temp) {
                $categories[] = [
                     'id' => strval($temp->external_id),
                ];
            }
        }
        return $categories;
    }
    
    protected function getTagsPayload($data)
    {
        $tags_string = explode(",", $data['tingmargid']);
        
        $tags = [];
        foreach (array_unique($tags_string) as $tag) {
            if (strlen(trim($tag)) > 0) {
                $temp = TempTag::select('external_id')->where('short_name', strtolower(trim($tag)))->where('language', $this->lang)->first();
                if ($temp) {
                    $tags[] = [
                         'id' => strval($temp->external_id),
                    ];
                }
            }
        }
        return $tags;
    }
    
    protected function getCreatePayload($data)
    {
        $images = $this->getImagesPayload($data);
        $categories = $this->getCategoriesPayload($data);
        $tags = $this->getTagsPayload($data);
        $omadused = $this->getOmadusedHtml($data);
        $upsell_ids = $this->getUpsellIds($data);
                
        $payload = [
            'sku' => strval($data['tootekood']),
            'name' => stripslashes($data['nimetus_est']),
            'status' => $this->getStatus($data['staatus']),
            'description' => $data['kirjeldus'],
            
            'price' => str_replace('.', ',', $data['hind']),
            'regular_price' => str_replace('.', ',', $data['hind']),
            
            'categories' => $categories,
            'tags' => $tags,
            
            'manage_stock' => true,
            'stock_quantity' => (($data['laoseis'] > 0) ? $data['laoseis'] : 0),
            //'stock_status' => ($data['laoseis'] > 0) ? 'instock' : 'outofstock',
            
            //'images' => $images,
            
            'meta_data' => [
                [
                    'key' => 'latin_name',
                    'value' => stripslashes($data['nimetus_lat']),
                ],
                [
                    'key' => 'markused_ja_lisainfo',
                    'value' => stripslashes($data['markused_ja_lisainfo_est']),
                ],
                [
                    'key' => 'liigikirjeldus',
                    'value' => stripslashes($data['liigikirjeldus']),
                ],
                [
                    'key' => 'omadus_1',
                    'value' => stripslashes($data['omadus_1']),
                ],
                [
                    'key' => 'vaartus_1',
                    'value' => stripslashes($data['vaartus_1']),
                ],
                [
                    'key' => 'omadus_2',
                    'value' => stripslashes($data['omadus_2']),
                ],
                [
                    'key' => 'vaartus_2',
                    'value' => stripslashes($data['vaartus_2']),
                ],
                [
                    'key' => 'omadus_3',
                    'value' => stripslashes($data['omadus_3']),
                ],
                [
                    'key' => 'vaartus_3',
                    'value' => stripslashes($data['vaartus_3']),
                ],
                [
                    'key' => 'omadus_4',
                    'value' => stripslashes($data['omadus_4']),
                ],
                [
                    'key' => 'vaartus_4',
                    'value' => stripslashes($data['vaartus_4']),
                ],
                [
                    'key' => 'omadused',
                    'value' => $omadused,
                ],
            ],
            
            'lang' => $this->lang,
        ];
        
        if ($images) {
            $payload['images'] = $images;
        }
        
        if ($upsell_ids) {
            $payload['upsell_ids'] = $upsell_ids;
        }
        
        //Log::channel('slack')->info('Create payload', ['update_stock' => $this->update_stock, 'stock_quantity' => isset($payload['stock_quantity']) ? $payload['stock_quantity'] : false]);
        
        return $payload;
    }
    
    protected function getUpdatePayload($product, $data)
    {
        $images = ($product['images']) ? null : $this->getImagesPayload($data);
        $categories = $this->getCategoriesPayload($data);
        $tags = $this->getTagsPayload($data);
        $omadused = $this->getOmadusedHtml($data);
        $upsell_ids = $this->getUpsellIds($data);
                
        $payload = [
            'name' => $data['nimetus_est'],
            'status' => $this->getStatus($data['staatus']),
            'description' => $data['kirjeldus'],
            
            'price' => str_replace('.', ',', $data['hind']),
            'regular_price' => str_replace('.', ',', $data['hind']),
            
            //'manage_stock' => true,
            
            'categories' => $categories,
            'tags' => $tags,
            
            'meta_data' => [
                [
                    'key' => 'latin_name',
                    'value' => stripslashes($data['nimetus_lat']),
                ],
                [
                    'key' => 'markused_ja_lisainfo',
                    'value' => stripslashes($data['markused_ja_lisainfo_est']),
                ],
                [
                    'key' => 'liigikirjeldus',
                    'value' => stripslashes($data['liigikirjeldus']),
                ],
                [
                    'key' => 'omadus_1',
                    'value' => stripslashes($data['omadus_1']),
                ],
                [
                    'key' => 'vaartus_1',
                    'value' => stripslashes($data['vaartus_1']),
                ],
                [
                    'key' => 'omadus_2',
                    'value' => stripslashes($data['omadus_2']),
                ],
                [
                    'key' => 'vaartus_2',
                    'value' => stripslashes($data['vaartus_2']),
                ],
                [
                    'key' => 'omadus_3',
                    'value' => stripslashes($data['omadus_3']),
                ],
                [
                    'key' => 'vaartus_3',
                    'value' => stripslashes($data['vaartus_3']),
                ],
                [
                    'key' => 'omadus_4',
                    'value' => stripslashes($data['omadus_4']),
                ],
                [
                    'key' => 'vaartus_4',
                    'value' => stripslashes($data['vaartus_4']),
                ],
                [
                    'key' => 'omadused',
                    'value' => $omadused,
                ],
            ],
        ];
        
        if ($images) {
            $payload['images'] = $images;
        }
        
        if ($upsell_ids) {
            $payload['upsell_ids'] = $upsell_ids;
        }
        
        if ($this->update_stock) {
            $payload['stock_quantity'] = (($data['laoseis'] > 0) ? $data['laoseis'] : 0);
            //$payload['stock_status'] = ($data['laoseis'] > 0) ? 'instock' : 'outofstock';
        }
        
        //Log::channel('slack')->info('Update payload', ['update_stock' => $this->update_stock, 'stock_quantity' => isset($payload['stock_quantity']) ? $payload['stock_quantity'] : false]);
        
        return $payload;
    }
    
    protected function getTranslationPayload($data, $lang, $main_product_id)
    {
        $language = $this->getLang($lang);
        //$categories = $this->getCategoriesPayload($data);
        
        return [
            'name' => stripslashes($data['nimetus_' . $language]),
            'type' => 'simple',
            'description' => $data['kirjeldus'],
            
            //'manage_stock' => true,
            //'stock_quantity' => (($data['laoseis'] > 0) ? $data['laoseis'] : 0),
            //'stock_status' => ($data['laoseis'] > 0) ? 'instock' : 'outofstock',
            
            //'categories' => $categories,
        
            'meta_data' => [
                [
                    'key' => 'latin_name',
                    'value' => stripslashes($data['nimetus_lat']),
                ],
                [
                    'key' => 'markused_ja_lisainfo',
                    'value' => stripslashes($data['markused_ja_lisainfo_' . $language]),
                ],
            ],
                        
            'lang' => $lang,
            'translation_of' => $main_product_id,
        ];
    }
    
    protected function getLang($lang)
    {
        $languages = [
            'et' => 'est',
            'en' => 'eng',
            'ru' => 'rus',
        ];
        
        return $languages[$lang];
    }
    
    protected function getStatus($status)
    {
        $status = array_search($status, config('importer.excel.status'));
        
        return $status;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Omadused
    |--------------------------------------------------------------------------
    */
    
    protected function getOmadusedHtml($data, $count = 4)
    {
        $html = '';
        //$html = "<div class=\"omadused-list-wrapper\">";
        $html .= "<ul class=\"omadused-list\">\n";
        
        for ($i = 1; $i <= $count; $i++) {
            if (strlen(trim($data["omadus_{$i}"])) > 0) {
                $html .= "\t<li class=\"omadused-list-item omadused-key-" . Str::slug($data["omadus_{$i}"]) . "\">\n";
                $html .= "\t\t<span class=\"omadused-list-item-key\">" . stripslashes(trim($data["omadus_{$i}"])) .  ": </span>\n";
                $html .= "\t\t<span class=\"omadused-list-item-value\">" . stripslashes(trim($data["vaartus_{$i}"])) .  "</span>\n";
                $html .= "\t</li>\n";
            }
        }
        
        $html .= "</ul>";
        //$html .= "</div>\n";
        
        return $html;
    }
    
    /*
    |--------------------------------------------------------------------------
    | If exists
    |--------------------------------------------------------------------------
    */
    
    public function ifTranslationExists($product, $lang)
    {
        return empty($product['translations']->$lang) ? false : $product['translations']->$lang;
    }
   
    /*
    |--------------------------------------------------------------------------
    | Methods: Get / Create / Update
    |--------------------------------------------------------------------------
    */
   
    private function ifProductExists($sku, $lang)
    {
        $product = Product::where('sku', strval($sku))->where('lang', $lang)->first();
        
        return empty($product['id']) ? false : $product;
    }
    
    private function getProduct($sku)
    {
        $product = Product::where('sku', strval($sku))->get();
        
        return empty($product[0]->id) ? false : collect($product[0]);
    }
    
    private function createProduct($data)
    {
        $product = Product::create($data);
        
        return empty($product['id']) ? false : $product;
    }
    
    private function updateProduct($product_id, $data)
    {
        $product = Product::update($product_id, $data);
        
        return empty($product['id']) ? false : $product;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Temp Product:  Update SKU
    |--------------------------------------------------------------------------
    */
   
    private function updateTempProduct($product, $lang)
    {
        $data = [
            'external_id' => empty($product['id']) ? null : $product['id'],
            'sku' => empty($product['sku']) ? null : $product['sku'],
            'language' => empty($product['lang']) ? null : $product['lang'],
            'name' => empty($product['name']) ? null : $product['name'],
            'date_created' => empty($product['date_created']) ? null : $product['date_created'],
            'date_modified' => empty($product['date_modified']) ? null : $product['date_modified'],
            'payload' => empty($product) ? null : json_encode($product),
        ];
        
        $temp_product = TempProduct::where('sku', $product['sku'])->where('language', $lang)->first();
    
        if ($temp_product) {
            $temp_product = TempProduct::where('sku', $product['sku'])->where('language', $lang)->update($data);
        } else {
            $temp_product = TempProduct::create($data);
        }
        
        return $temp_product;
    }
    
    private function createTempProduct($product, $lang)
    {
        $data = [
            'external_id' => empty($product['id']) ? null : $product['id'],
            'sku' => empty($product['sku']) ? null : $product['sku'],
            'type' => empty($product['type']) ? null : $product['type'],
            'language' => empty($product['lang']) ? null : $product['lang'],
            'name' => empty($product['name']) ? null : $product['name'],
            'date_created' => empty($product['date_created']) ? null : $product['date_created'],
            'date_modified' => empty($product['date_modified']) ? null : $product['date_modified'],
            'payload' => empty($product) ? null : json_encode($product),
        ];
        
        $temp_product = TempProduct::where('sku', $product['sku'])->where('language', $lang)->first();
    
        if (!$temp_product) {
            $temp_product = TempProduct::create($data);
        }
        
        return $temp_product;
    }
    
    /*
    |--------------------------------------------------------------------------
    | Init: Update product
    |--------------------------------------------------------------------------
    */
    
    public function send()
    {
        $product = $this->product;
                
        if ($product) {
            //dd($this->getUpdatePayload($this->data));
            $this->updateTempProduct($product, $this->lang);
            $product = $this->updateProduct($product['id'], $this->getUpdatePayload($product, $this->data));
        } else {
            //dd($this->getCreatePayload($this->data));
            $product = $this->createProduct($this->getCreatePayload($this->data));
            $this->createTempProduct($product, $this->lang);
        }
        
        if ($product) {
            $translations = [];
            foreach ($this->translation_langs as $lang) {
                $translation_id = $this->ifTranslationExists($product, $lang);
                if ($translation_id) {
                    try {
                        $translations[$lang] = $this->updateProduct($translation_id, $this->getTranslationPayload($this->data, $lang, $product['id']));
                    } catch (\Exception $e) {
                        Log::channel('slack')->error('PostProduct error - Translation problem', collect($this->getTranslationPayload($this->data, $lang, $product['id']))->toArray());
                    }
                } else {
                    try {
                        $translations[$lang] = $this->createProduct($this->getTranslationPayload($this->data, $lang, $product['id']));
                    } catch (\Exception $e) {
                        Log::channel('slack')->error('PostProduct error - Translation problem', collect($this->getTranslationPayload($this->data, $lang, $product['id']))->toArray());
                    }
                }
            }
        } else {
            Log::channel('slack')->error('PostProduct error - Product not exists', collect($this->product)->toArray());
        }
        
        $this->timerStop();
        $time = $this->getTimer();
        
        return [
            'time' => $time,
            'product' => $product,
            'translations' => isset($translations) ? $translations : [],
        ];
    }
}
