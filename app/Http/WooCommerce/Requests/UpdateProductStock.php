<?php

namespace App\Http\WooCommerce\Requests;

use App\Http\WooCommerce\Requests\Traits\TimerTrait;

class UpdateProductStock
{
    use TimerTrait;

    public $data;

    public $langs;
    public $lang;
    public $translation_langs;

    protected $timerName = 'update_product_stock';

    public function __construct(Collection $data)
    {
        $this->data = $data;
        
        if (empty($this->data['tootekood'])) {
            Log::channel('slack')->error('PostProduct error - $data["tootekood"] missing', collect($this->data)->toArray());
        }
        
        $this->langs = config('importer.language.list');
        $this->lang = config('importer.language.default');
        $this->translation_langs = array_remove_value(config('importer.language.list'), config('importer.language.default'));
        
        $this->timerStart();
        
        $this->product = $this->ifProductExists($this->data['tootekood'], $this->lang);
    }
    
    public function send()
    {
        $this->timerStop();
        $time = $this->getTimer();
        
        return [
            'time' => $time,
            'product' => $product,
            'translations' => isset($translations) ? $translations : [],
        ];
    }
}
