<?php

namespace App\Http\WooCommerce\Requests\Traits;

use App\Support\Timer;
use Illuminate\Support\Str;

trait TimerTrait
{
    protected function getTimerName()
    {
        if (isset($this->timerName)) {
            return $this->timerName;
        } else {
            return Str::slug(get_class($this));
        }
    }
    
    protected function timerStart()
    {
        Timer::start($this->getTimerName());
    }
    
    protected function timerStop()
    {
        Timer::stop($this->getTimerName());
    }
    
    protected function getTimer()
    {
        $timer = Timer::get($this->getTimerName());
        $time = Str::beforeLast($timer['time'], '.');
        
        return $time;
    }
}
