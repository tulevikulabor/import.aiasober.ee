<?php

namespace App\Http\Middleware;

use Closure;
use Log;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->isAdmin()) {
                return $next($request);
            } else {
                return response('Unauthorized', 401);
            }
        } else {
            return response('Unauthorized', 401);
            //return response(view('admin.auth.user_not_authorized'), 401);
        }
    }
}
