<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App;
use URL;
use Cookie;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $locale = $this->getLocaleByIP($request);
        //
        // if (!$locale) {
            $locale = (isset($request->locale) && array_key_exists($request->locale, config('locale.locales')))?$request->locale:\App::getLocale();
        // }

        App::setLocale($locale);

        $request->session()->put('locale', $locale);
        Cookie::queue('locale', $locale);

        URL::defaults([
            'locale' => $locale, // $request->route()->parameter('locale'),
        ]);
        // Remove these params so they aren't passed to controllers.
        $request->route()->forgetParameter('locale');

        return $next($request);
    }

    public function getLocaleByIP($request)
    {
        if (config('app.env') == 'local') {
            return config('app.locale');
        } else {
            $url = 'http://ip-api.com/json/'.$request->ip();

            $client = new Client();
            $result = $client->request('GET', $url);

            $response = json_decode($result->getBody());

            return strtolower($response->countryCode);
        }
    }
}
