<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\SyncProductsStarted;
use App\Events\SyncProductsFinished;
use App\Listeners\SendSyncProdcutsStartNotification;
use App\Listeners\SendSyncProdcutsFinishNotification;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SyncProductsStarted::class => [
            SendSyncProdcutsStartNotification::class,
        ],
        SyncProductsFinished::class => [
            SendSyncProdcutsFinishNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
