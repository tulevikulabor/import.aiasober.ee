<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Blade;
use Spatie\Menu\Laravel\Menu;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::share('locale', app()->getLocale());
        
        if (!$this->app->environment('production')) {
            $this->app->register(\Laravel\Dusk\DuskServiceProvider::class);
        }
        
        Blade::directive('pushonce', function ($expression) {
            $isDisplayed = '__pushonce_'.trim(substr($expression, 2, -2));
            return "<?php if(!isset(\$__env->{$isDisplayed})): \$__env->{$isDisplayed} = true; \$__env->startPush{$expression}; ?>";
        });
        
        Blade::directive('endpushonce', function ($expression) {
            return '<?php $__env->stopPush(); endif; ?>';
        });
        
        Paginator::useBootstrap();
        
        Menu::macro('main', function () {
            
            $user = auth()->user();
            $is_superuser = $user->isSuperuser();
            $is_admin = $user->isAdmin();
            
            return Menu::new()
                ->routeIf($is_admin, 'index', __('Dashboard'))
                ->routeIf($is_admin, 'imports.index', __('Imports'))
                ->routeIf($is_admin, 'images.index', __('Images'))
                ->routeIf($is_admin, 'exports.index', __('Exports'))
                ->routeIf($is_superuser, 'exports.products', __('Products'))
                ->routeIf($is_superuser, 'woocommerce.products.table', __('Products (WC)'))
                ->routeIf($is_superuser, 'woocommerce.products.database', __('DB'))
                ->routeIf($is_admin, 'users.index', __('Users'))
                ->routeIf($is_superuser, 'woocommerce.products.index', __('API:JSON'))
                ->routeIf($is_superuser, 'woocommerce.products.download-test-excel', __('Test Excel'))
                ->setActiveFromRequest()
                ->addClass('main-menu navbar-nav mx-auto');
        });
    }
}
