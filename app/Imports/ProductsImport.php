<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithLimit;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ProductsImport implements ToCollection, WithMultipleSheets, WithValidation, WithHeadingRow, WithLimit
{
    use Importable;
    
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
    }
    
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }
    
    public function rules(): array
    {
        return [];
    }
    
    public function batchSize():int
    {
        return 50000;
    }

    public function chunkSize(): int
    {
        return 50000;
    }

    public function limit(): int
    {
        return 50000;
    }
}
