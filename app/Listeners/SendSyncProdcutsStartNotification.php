<?php

namespace App\Listeners;

use App\Events\SyncProductsStarted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Log;

class SendSyncProdcutsStartNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SyncProductsStarted  $event
     * @return void
     */
    public function handle(SyncProductsStarted $event)
    {
        Log::channel('slack')->info('Product synchronization has started', collect($event)->toArray());
    }
}
