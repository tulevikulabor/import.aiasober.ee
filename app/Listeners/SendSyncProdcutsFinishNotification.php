<?php

namespace App\Listeners;

use App\Events\SyncProductsFinished;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Log;

class SendSyncProdcutsFinishNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SyncProductsFinished  $event
     * @return void
     */
    public function handle(SyncProductsFinished $event)
    {
        Log::channel('slack')->info('Product synchronization is complete', collect($event)->toArray());
    }
}
