<?php

namespace App\Listeners;

use App\Events\SyncProductsPageChanged;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendSyncProdcutsPageChangeNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SyncProductsPageChanged  $event
     * @return void
     */
    public function handle(SyncProductsPageChanged $event)
    {
        //
    }
}
