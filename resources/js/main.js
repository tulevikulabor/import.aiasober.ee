/*----------------------------------------------
Bootstrap: Carousel
-----------------------------------------------*/

$(function () {
    $('.carousel').carousel(
        /*{
                interval: 5000,
                keyboard: true,
                pause: 'hover',
                ride: false,
                wrap: true,
                touch: true,
            }*/
    );

    $('.carousel-control-next').carousel('next');
    $('.carousel-control-prev').carousel('prev');
});

/*----------------------------------------------
Bootstrap: Collapse
-----------------------------------------------*/

/* $(function () {
    $('.collapse').collapse({
        toggle: false,
    });
}); */

/*----------------------------------------------
jQuery: FixTo
-----------------------------------------------*/

$(function () {
    $('#header-menu-bar').fixTo('body', {
        zIndex: 20,
        useNativeSticky: false,
    });
});

/*----------------------------------------------
Bootstrap: Collapse
-----------------------------------------------*/

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="tooltip"]').on('shown.bs.tooltip', function () {
        $('.tooltip').addClass('animated expand');
    });
});

/*----------------------------------------------
Main menu: Modal
-----------------------------------------------*/

/* $(function () {
    $('#main-menu-modal').modal('show');
}); */

/*----------------------------------------------
Header menu: Dropdown
-----------------------------------------------*/

$(function () {
    $(".header-menu-dropdown").mouseover(function () {
        $('.dropdown-toggle', this).addClass('dropdown-toggle-hover');
        $(this).addClass('show').attr('aria-expanded', "true");
        $(this).find('.dropdown-menu').addClass('show');
    }).mouseout(function () {
        $('.dropdown-toggle', this).removeClass('dropdown-toggle-hover');
        $(this).removeClass('show').attr('aria-expanded', "false");
        $(this).find('.dropdown-menu').removeClass('show');
    });

    $('.header-menu-dropdown .dropdown-toggle').click(function () {
        var location = $(this).attr('href');
        window.location.href = location;
        return false;
    });
});