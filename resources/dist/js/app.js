window.backAway = function (e) {
  if (document.referrer) {
    window.location.href = document.referrer;
    return;
  }

  window.location.pathname = '/';
};

window.goBack = function (e) {
  var defaultLocation = window.location.origin;
  var oldHash = window.location.hash; //alert('URL: ' + defaultLocation);

  history.back(); // Try to go back

  var newHash = window.location.hash;
  /* If the previous page hasn't been loaded in a given time (in this case
   * 1000ms) the user is redirected to the default location given above.
   * This enables you to redirect the user to another page.
   *
   * However, you should check whether there was a referrer to the current
   * site. This is a good indicator for a previous entry in the history
   * session.
   *
   * Also you should check whether the old location differs only in the hash,
   * e.g. /index.html#top --> /index.html# shouldn't redirect to the default
   * location.
   */

  if (newHash === oldHash && (typeof document.referrer !== "string" || document.referrer === "")) {
    window.setTimeout(function () {
      // redirect to default location
      window.location.href = defaultLocation;
    }, 1000); // set timeout in ms
  }

  if (e) {
    if (e.preventDefault) e.preventDefault();
    if (e.preventPropagation) e.preventPropagation();
  }

  return false; // stop event propagation and browser default event
};

$(function () {
  /* $('.modal').modal({
         backdrop: true,
         keyboard: true,
         focus: true,
     }); */
  // $('#symbols-modal').modal('show');
  $('.modal-backdrop').click(function () {
    $('.modal').modal('hide');
  });
});
$(window).on("load", function (e) {
  setTimeout(function () {
    // $('.preloader').css('cursor', 'pointer');
    // $('.preloader').hide();
    $('.preloader').addClass('hide');
  }, 300);
});
/*----------------------------------------------
Bootstrap: Carousel
-----------------------------------------------*/

$(function () {
  $('.carousel').carousel();
  $('.carousel-control-next').carousel('next');
  $('.carousel-control-prev').carousel('prev');
});
/*----------------------------------------------
Bootstrap: Collapse
-----------------------------------------------*/

/* $(function () {
    $('.collapse').collapse({
        toggle: false,
    });
}); */

/*----------------------------------------------
jQuery: FixTo
-----------------------------------------------*/

$(function () {
  $('#header-menu-bar').fixTo('body', {
    zIndex: 20,
    useNativeSticky: false
  });
});
/*----------------------------------------------
Bootstrap: Collapse
-----------------------------------------------*/

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="tooltip"]').on('shown.bs.tooltip', function () {
    $('.tooltip').addClass('animated expand');
  });
});
/*----------------------------------------------
Main menu: Modal
-----------------------------------------------*/

/* $(function () {
    $('#main-menu-modal').modal('show');
}); */

/*----------------------------------------------
Header menu: Dropdown
-----------------------------------------------*/

$(function () {
  $(".header-menu-dropdown").mouseover(function () {
    $('.dropdown-toggle', this).addClass('dropdown-toggle-hover');
    $(this).addClass('show').attr('aria-expanded', "true");
    $(this).find('.dropdown-menu').addClass('show');
  }).mouseout(function () {
    $('.dropdown-toggle', this).removeClass('dropdown-toggle-hover');
    $(this).removeClass('show').attr('aria-expanded', "false");
    $(this).find('.dropdown-menu').removeClass('show');
  });
  $('.header-menu-dropdown .dropdown-toggle').click(function () {
    var location = $(this).attr('href');
    window.location.href = location;
    return false;
  });
});
