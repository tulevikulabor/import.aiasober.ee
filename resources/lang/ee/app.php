<?php

return [
    'env' => [
        'local' => 'Lokaalne',
        'development' => 'Arendus',
        'staging' => 'Pre-live',
        'production' => 'Live',
    ],
    'copyright' => 'Aiasõber OÜ Kõik õigused kaitstud © :year',
];
