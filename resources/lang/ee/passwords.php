<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Sinu salasõna on taastatud!',
    'sent' => 'Loe oma e-maili, et kinnitada salasõna vahetus!',
    'throttled' => 'Palun oota enne kui uuesti üritad.',
    'token' => 'Selle salasõna võti on vigane.',
    'user' => "Sellist kasutajat ei eksisteeri.",

];
