<?php

return [
    'env' => [
        'local' => 'Local',
        'development' => 'Development',
        'staging' => 'Pre-live',
        'production' => 'Live',
    ],
    'copyright' => 'Aiasõber OÜ Kõik õigused kaitstud © :year',
];
