<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Importer Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by importer.
    |
    */

    'exports' => [
        'download' => [
            'temporary_notice' => '<strong>NB!</strong> Excel on veel täiendamisel. Pisut kannatust!<br />Võib tutvuda, kuid tööfailina veel mitte kasutada!<br /><strong>Tule mõne aja pärast siia tagasi.</strong>',
        ],
    ],
    
    'imports' => [
        'state' => [
            'pending' => 'Pending',
            'active' => 'Active',
            'inactive' => 'Inactive',
            'file_error' => 'Incorrect file',
        ],
    ],
];
