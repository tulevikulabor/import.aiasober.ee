@extends('layouts.login')

@section('content')
<form method="POST" action="{{ route('login') }}" class="form-signin text-center mb-2 mb-md-5">
    @csrf

    <h1 class="h3 mb-3">{{ __('Login') }}</h1>

    @error('email')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror

    @error('password')
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror

    <label for="inputEmail" class="sr-only">{{ __('E-Mail Address') }}</label>
    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-Mail Address') }}">
    <label for="inputPassword" class="sr-only">{{ __('Password') }}</label>
    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
    <div class="checkbox mt-2 mt-md-3 mb-2 mb-md-4">
        <label for="remember">
            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} class="mr-1"> {{ __('Remember Me') }}
        </label>
    </div>
    <button class="w-100 btn btn-lg btn-main" type="submit">{{ __('Login') }}</button>
    <label class="mt-2 mt-md-5 mb-0 mb-md-5">
        @if (Route::has('password.request'))
        <a href="{{ route('password.request') }}">
            {{ __('Forgot Your Password?') }}
        </a>
        @endif
    </label>
</form>
@endsection