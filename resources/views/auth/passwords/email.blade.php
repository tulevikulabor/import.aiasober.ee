@extends('layouts.login')

@section('content')
<form method="POST" action="{{ route('password.email') }}" class="form text-center mb-2 mb-md-5">
    @csrf

    <h1 class="mb-3">{{ __('Reset Password') }}</h1>

    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif

    <div class="form-group d-flex align-items-center row">
        <label for="email" class="col-md-3 col-form-label text-left text-md-right">{{ __('E-Mail Address') }}</label>

        <div class="col-md-9">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

            @error('email')
            <span class="invalid-feedback" role="alert">
                {{ $message }}
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-9 offset-md-3">
            <button type="submit" class="btn btn-main">
                {{ __('Saada link e-postile') }}
            </button>
        </div>
    </div>

    <label class="mt-3 mt-md-5 mb-0 mb-md-5">
        @if (Route::has('login'))
        <a href="{{ route('login') }}">
            {{ __('Logi sisse') }}
        </a>
        @endif
    </label>
</form>
@endsection