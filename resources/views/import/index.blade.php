@extends('layouts.app')

@section('title')
{{ __('Imports') }}
@endsection

@section('content')

@component('components.card', ['title' => __('Pending import files')])
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">{{ __('ID') }}</th>
                <th scope="col">{{ __('Original filename') }}</th>
                <th scope="col">{{ __('State') }}</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pending_import_files as $import_file)
            <tr>
                <th scope="row">{{ $import_file->id }}</th>
                <td><a href="{{ route('imports.download.excel', ['import_file' => $import_file]) }}">{{ $import_file->original_filename }}</a></td>
                <td class="d-flex align-items-center">
                    @if($import_file->state == 'active')
                        <div class="spinner-border text-success spinner-border-sm mr-1" role="status"></div>
                    @endif
                    {{ __('importer.imports.state.'.$import_file->state) }}
                </td>
                <td align="right">
                    @component('components.dropdown', ['title' => __('Action'),
                        'items' => [
                            [
                                'name' => __('Run import (with stock update)'),
                                'url' => route('imports.run', ['import_file' => $import_file, 'update_stock' => true]),
                                'disabled' => !in_array($import_file->state, ['pending']),
                                'display' => true,
                            ],
                            [
                                'name' => __('Run import (with no stock update)'),
                                'url' => route('imports.run', ['import_file' => $import_file, 'update_stock' => false]),
                                'disabled' => !in_array($import_file->state, ['pending']),
                                'display' => true,
                            ],
                            [
                                'name' => __('Cancel import'),
                                'url' => route('imports.cancel', ['import_file' => $import_file]),
                                'disabled' => in_array($import_file->state, ['pending']),
                                'display' => true,
                            ],
                            [
                                'name' => __('Trash'),
                                'url' => route('imports.trash', ['import_file' => $import_file]),
                                'disabled' => !in_array($import_file->state, ['pending']),
                                'display' => true,
                            ]
                        ],
                    ])
                    @endcomponent
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
{{ $pending_import_files->withQueryString()->onEachSide(3)->links() }}
@endcomponent

@component('components.card', ['title' => __('Products import in progress'), 'spinner' => ['id' => 'spinner-import-products-active-session-progress', 'active' => true]])
<x-progressbar id="import-products-active-session-progress" spinnerId="spinner-import-products-active-session-progress" value="0" valuemin="0" valuemax="0" :isPercentage="true" :url="route('api.import-sessions.active-session.progress')" interval="5" />
@endcomponent

@component('components.card', ['title' => __('Imports history')])
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">{{ __('Name') }}</th>
                <th scope="col">{{ __('Started at') }}</th>
                <th scope="col">{{ __('Cancelled at') }}</th>
                <th scope="col">{{ __('Finished at') }}</th>
                <th scope="col">{{ __('Products') }}</th>
                <th scope="col">{{ __('Pages') }}</th>
                <th scope="col">{{ __('Batch jobs statistics') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($job_batches as $job_batch)
            <tr>
                <th scope="row">{{ $job_batch->name }}</th>
                <td>{{ $job_batch->created_at->format('d.m.Y - H:i') }}</th>
                <td>{{ ($job_batch->cancelled_at) ? now()->createFromTimestamp($job_batch->cancelled_at)->format('d.m.Y - H:i') : '-' }}</td>
                <td>{{ ($job_batch->finished_at) ? now()->createFromTimestamp($job_batch->finished_at)->format('d.m.Y - H:i') : '-' }}</td>
                <td>~ {{ ($job_batch->total_jobs - $job_batch->pending_jobs + $job_batch->failed_jobs) * 10 }} tk</td>
                <td><strong>{{ $job_batch->total_jobs - $job_batch->pending_jobs + $job_batch->failed_jobs }}</strong> / {{ $job_batch->total_jobs }}</td>
                <td nowrap="nowrap">
                    <span class="badge badge-pill badge-info ml-3 px-3 py-2">{{ $job_batch->total_jobs ?? 0 }}</span>
                    <span class="badge badge-pill badge-success ml-3 px-3 py-2">{{ ($job_batch->total_jobs - $job_batch->pending_jobs) ?? 0 }}</span>
                    <span class="badge badge-pill badge-warning ml-3 px-3 py-2">{{ $job_batch->pending_jobs ?? 0 }}</span>
                    <span class="badge badge-pill badge-danger ml-3 px-3 py-2">{{ $job_batch->failed_jobs ?? 0 }}</span>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
{{ $job_batches->withQueryString()->onEachSide(3)->links() }}
@endcomponent

@component('components.card', ['title' => __('Inactive import files')])
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">{{ __('ID') }}</th>
                <th scope="col">{{ __('Original filename') }}</th>
                <th scope="col">{{ __('Laoseisu uuendamine') }}</th>
                <th scope="col">{{ __('State') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($inactive_import_files as $import_file)
            <tr>
                <th scope="row">{{ $import_file->id }}</th>
                <td><a href="{{ route('imports.download.excel', ['import_file' => $import_file]) }}">{{ $import_file->original_filename }}</a></td>
                <td>{{ __($import_file->update_stock ? 'Yes' : 'No') }}</td>
                <td>{{ __('importer.imports.state.'.$import_file->state) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
{{ $inactive_import_files->withQueryString()->onEachSide(3)->links() }}
@endcomponent

@endsection