@extends('layouts.app')

@section('title')
{{ __('Users') }}
@endsection

@section('content')

@component('components.card', ['title' => __('Users')])
<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">{{ __('Name') }}</th>
            <th scope="col">{{ __('E-mail') }}</th>
            <th scope="col">{{ __('Created') }}</th>
            <th scope="col">{{ __('Is admin?') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $key => $user)
        <tr>
            <th scope="row">{{ $user->name }}</th>
            <td>{{ $user->email }}</td>
            <td>{{ date('d.m.Y H:i:s', strtotime($user->created_at)) }}</td>
            <td>{{ boolean($user->is_admin) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endcomponent

@endsection