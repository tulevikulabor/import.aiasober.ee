@extends('layouts.app')

@section('title')
    {{ __('Exports') }}
@endsection

@section('content')

@component('components.card', ['title' => __('Export to Excel')])
    <div class="text-center">
        <a href="{{ route('exports.download.excel') }}" class="btn btn-main mr-1 my-1 my-sm-0">{{ __('Download Excel') }}</a>
        <a href="{{ route('exports.download.csv') }}" class="btn btn-main my-1 my-sm-0">{{ __('Download CSV') }}</a>
    </div>
@endcomponent

@component('components.card', ['title' => __('Product sync in progress')])
    <x-progressbar id="sync-products-active-session-progress" value="0" valuemin="0" valuemax="0" :isPercentage="true" :url="route('api.sync-sessions.active-session.progress')" interval="5" />
@endcomponent

@component('components.card', ['title' => __('Exports history')])
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">{{ __('Started at') }}</th>
                    <th scope="col">{{ __('Finished at') }}</th>
                    <th scope="col">{{ __('Progress at') }}</th>
                    <th scope="col">{{ __('Products') }}</th>
                    <th scope="col">{{ __('Pages') }}</th>
                    <!--<th scope="col">{{ __('Export file') }}</th>-->
                </tr>
            </thead>
            <tbody>
                @foreach ($sync_sessions as $sync_session)
                <tr>
                    <th scope="row">{{ date('d.m.Y - H:i', strtotime($sync_session->started_at)) }}</th>
                    <td>{{ ($sync_session->finished_at) ? date('d.m.Y - H:i', strtotime($sync_session->finished_at)) : '-' }}</td>
                    <td>{{ date('d.m.Y - H:i', strtotime($sync_session->page_changed_at)) }}</td>
                    <td>{{ $sync_session->products }}</td>
                    <td><strong>{{ $sync_session->page }}</strong> / {{ $sync_session->pages }}</td>
                    <!--<td>
                        @if(file_exists($sync_session->export_file))
                            <a href="{{ $sync_session->export_file }}" class="btn btn-main">{{ __('Excel') }}</a>
                            @else
                            -
                            @endif
                    </td>-->
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $sync_sessions->links() }}
@endcomponent

@endsection