@extends('layouts.app')

@section('title')
    {{ __('Products') }}
@endsection

@section('content', [
    'title' => __('Products'),
    'wide' => true,
    'badges' => [
        [
            'name' => $meta['total_results'],
            'color' => 'primary',
        ],
        [
            'name' => parse_url(config('woocommerce.store_url'), PHP_URL_HOST),
            'color' => 'info',
        ],
        [
            'name' => $time . 's',
            'color' => 'warning',
        ],
    ]
])
<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">{{ __('ID') }}</th>
                <th scope="col">{{ __('Product name') }}<img src="{{ asset('assets/img/flags/ee.svg') }}" class="ml-2" alt="ET" height="15" /></th>
                @foreach ($languages as $language)
                    <th scope="col">{{ __('Product name') }}<img src="{{ asset('assets/img/flags/' . strtoupper($language) . '.svg') }}" class="ml-2" alt="{{ strtoupper($language) }}" height="15" /></th>
                @endforeach
                <th scope="col">{{ __('SKU') }}</th>
                <th scope="col">{{ __('Regular price') }}</th>
                <th scope="col">{{ __('Price') }}</th>
                <th scope="col">{{ __('Sale Price') }}</th>
                <th scope="col">{{ __('Categories') }}</th>
                <th scope="col">{{ __('Photos') }}</th>
                <th scope="col">{{ __('Description') }}<img src="{{ asset('assets/img/flags/ee.svg') }}" class="ml-2" alt="EST" height="15" /></th>
                <th scope="col">{{ __('Description') }}<img src="{{ asset('assets/img/flags/ru.svg') }}" class="ml-2" alt="RUS" height="15" /></th>
                <th scope="col">{{ __('Description') }}<img src="{{ asset('assets/img/flags/en.svg') }}" class="ml-2" alt="ENG" height="15" /></th>
                <th scope="col">{{ __('Created') }}</th>
                <th scope="col">{{ __('Updated') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $key => $product)
            <tr>
                <th scope="row"><span class="badge badge-pill badge-light px-3 py-2 text-muted">{{ $product->id }}</span></th>
                <td><a href="{{ $product->permalink }}" target="_blank">{{ $product->name }}</a></td>
                @foreach ($languages as $language)
                    <td><a href="{{ $translations[$product->id][$language]['permalink'] }}" target="_blank">{{ $translations[$product->id][$language]['name'] }}</a></td>
                @endforeach
                <td><span class="badge badge-pill badge-light px-3 py-2">{{ $product->sku }}</span></td>
                <td>{{ $product->regular_price }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->sale_price }}</td>
                <td>
                    @foreach ($product->categories as $key => $category)
                    {{ $category->name }};
                    @endforeach
                </td>
                <td>
                    @foreach ($product->images as $key => $image)
                    <a href="{{ $image->src }}" target="_blank">{{ $image->name }}</a>;
                    @endforeach
                </td>
                <td>{{ read_more(strip_tags(html_entity_decode($product->description)), 30) }}</td>
                @foreach ($languages as $language)
                    <td>{{ read_more(strip_tags(html_entity_decode($translations[$product->id][$language]['description'])), 30) }}</td>
                @endforeach
                <td><span class="badge badge-pill badge-light px-3 py-2 text-muted">{{ date('d.m.Y H:i:s', strtotime($product->date_created)) }}</span></td>
                <td><span class="badge badge-pill badge-light px-3 py-2 text-muted">{{ date('d.m.Y H:i:s', strtotime($product->date_modified)) }}</span></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @isset($meta)
        @component('components.pagination', [
            'total_results' => $meta['total_results'],
            'total_pages' => $meta['total_pages'],
            'current_page' => $meta['current_page'],
            'previous_page' => $meta['previous_page'],
            'next_page' => $meta['next_page'],
            'first_page' => $meta['first_page'],
            'last_page' => $meta['last_page'],
        ])
        @endcomponent
    @endisset
</div>

@endsection