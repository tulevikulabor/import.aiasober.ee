@extends('layouts.login')

@section('content')
    <div class="form-signin text-center mb-2 mb-md-5">

        <h1 class="h3 mb-3">{{ __('Download Excel') }}</h1>

        <div class="text-center">
            <p>{!! __('importer.exports.download.temporary_notice') !!}</p>
            <a href="{{ route('exports.download.excel.hash') }}" class="btn btn-main mr-2">{{ __('Download Excel') }}</a>
        </div>

    </div>
@endsection