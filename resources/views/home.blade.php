@extends('layouts.app')

@section('title')
{{ __('Welcome!') }}
@endsection

@section('content')

@php /*
@component('components.card', ['title' => __('Dashboard')])
{{ __('You are logged in!') }}
@endcomponent
*/
@endphp

@php /*
@component('components.card', ['title' => __('Excel file')])
<div class="text-center">
    @if(isset($time))
    <p>{{ __('Last reload') }}: {{ $time ?? '' }}</p>
    @endif
    <a href="{{ route('importer.reload') }}" class="btn btn-main">{{ __('Reload file') }}</a>
</div>
@endcomponent
*/
@endphp

@component('components.card', ['title' => __('Upload'), 'acc'])

@component('components.dropzone_fullscreen', [
'paramName' => 'file', // The name that will be used to transfer the file
'url' => route('upload.file'), // Upload route
'maxFiles' => 3000,
'maxFilesize' => 50, // MB
'acceptedFiles' => \Arr::collapse(config('importer.mime')),
])

@endcomponent

@endcomponent

@component('components.card', ['title' => __('Upload manual')])

<ul class="list-group">
    <li class="list-group-item d-flex align-items-center">

        <span class="badge badge-primary badge-pill mr-3">1</span>
        <div>Tiri või uploadi nii pildid kui importfail(id) üleval pool asuvasse üleslaadimise vormile.<br />Pildid ja import failid võivad olla segamini.</div>
    </li>
    <li class="list-group-item d-flex align-items-center">

        <span class="badge badge-primary badge-pill mr-3">2</span>
        <div>Lae alati enne importfaili laadimist üles kõik pildid, siis ei pea mitu korda importfaili laadimist käivitama.<br />Piltide olemasolus saad veenduda lehel <a href="/images" alt="Fotod" class="font-weight-bold">"Fotod"</a>.</div>
    </li>
    <li class="list-group-item d-flex align-items-center">

        <span class="badge badge-primary badge-pill mr-3">3</span>
        <div>Kui kõik pildid on üleslaetud, siis käivita import fail(id) lehel <a href="/imports" alt="Impordid" class="font-weight-bold">"Impordid"</a>.</div>
    </li>
</ul>

@endcomponent

@endsection