<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Importer') }}</title>

    <!-- Favicons -->
    <link rel="icon" type="image/svg+xml" href="{{ asset('assets/img/favicon-round.svg') }}">
    <link rel="apple-touch-icon" type="image/svg+xml" href="{{ asset('assets/img/favicon.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/bootstrap.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    
    <!-- Styles -->
    @stack('styles')
    
</head>