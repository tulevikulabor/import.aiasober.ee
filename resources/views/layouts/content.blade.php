@include('layouts.header')

    @include('partials.flash_messages')

    @yield('content')

@include('layouts.footer')