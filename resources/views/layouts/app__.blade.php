<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Brittel | Importer</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/bootstrap.min.css') }}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">

</head>

<body class="main">

    <header>
        <div class="container-fluid d-flex align-items-center">
            <img src="{{ asset('assets/img/logo.svg') }}" class="ml-auto" alt="Brittel" height="25" />
        </div>
    </header>

    <main class="h-100">
        <div class="container">

            <h1>@yield('content')</h1>

            @yield('content')

        </div>
    </main>

    <footer>
        <p class="copyright">Brittel Bötker - Kõik õigused kaitstud © 2021</p>
        @include('partials.ribbon')
    </footer>

    <script src="{{ asset('assets/vendor/jquery/jquery.slim.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>

</body>

</html>