<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.head')

<body class="env-{{ config('app.env') }}">

    <main class="login-screen h-100">

        <div class="row h-100 m-0 p-0">
            <section class="login-screen-left col col-12 col-md-5 d-none d-md-block">
                <div class="login-screen-left-content">
                    <h2>{!! str_replace(' | ', '<span> | </span>', config('app.name', 'Importer')) !!}</h2>
                    <p>{{ __('app.copyright', ['year' => date('Y')]) }}</p>
                </div>
            </section>

            <section class="login-screen-right col col-12 col-md-7 d-flex align-items-center align-items-start flex-column">
                <header class="w-100">
                    <div class="container-fluid p-3 p-md-4 d-flex align-items-center">
                        <a href="{{ config('woocommerce.store_url') }}" class="btn btn-main">{{ __('<') }}</a>
                        <img src="{{ asset('assets/img/logo.svg') }}" class="ml-auto" alt="Brittel" height="45" />
                    </div>
                </header>
                <main class="my-auto">
                    @yield('content')
                </main>
                <footer>
                    <p class="copyright mt-0 mt-md-5 mb-1 mb-md-3 d-md-none">{{ __('app.copyright', ['year' => date('Y')]) }}</p>
                    @include('partials.ribbon')
                </footer>
                
                @include('partials.preloader')

            </section>

        </div>

    </main>
    
    <script src="{{ asset('assets/vendor/jquery/jquery.slim.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>

</body>

</html>