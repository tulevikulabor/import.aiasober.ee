<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.head')

<body class="env-{{ config('app.env') }}">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light fixed-top shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- <img src="{{ asset('assets/img/logo_white.svg') }}" class="mb-1" alt="Brittel" height="24" />-->
                    {!! str_replace(' | ', '<span> | </span>', Str::beforeLast(config('app.name', 'Importer'), '|')) !!}
                </a>
                <button class="navbar-toggler border-0" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="hamburger white">
                        <i></i><i></i><i></i>
                    </span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    {!! Menu::main() !!}

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav">
                        <!-- Authentication Links -->
                        @guest
                        @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @endif

                        @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @endif
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right py-0" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item py-2 rounded" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @if(View::hasSection('content-wide'))
            <main class="py-4">
                <div class="container-fluid mt-5 pt-2 pb-2">
                    <h1>@yield('title')</h1>
                </div>

                <div class="container-fluid">
                    @include('partials.flash_messages')
                </div>

                @yield('content-wide')

            </main>
        @else
            <main class="py-4">
                <div class="container mt-5 pt-2 pb-2">
                    <h1>@yield('title')</h1>
                </div>

                <div class="container">
                    @include('partials.flash_messages')
                </div>

                @yield('content')

            </main>
        @endif
        
        @include('partials.ribbon')
        @include('partials.preloader')

    </div>

    <!-- Javascript -->
    <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('assets/vendor/bootstrap/bootstrap.bundle.min.js') }}"></script>

    <!-- Vendor -->
    <script src="{{ asset('assets/js/vendor.js') }}"></script>
    <!-- Custom -->
    <script src="{{ asset('assets/js/app.js') }}"></script>
    
    <!-- Blade component -->
    @stack('scripts')

</body>

</html>