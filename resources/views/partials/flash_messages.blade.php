@if (session('message'))
    <div class="alert alert-success animated fadeIn text-center" role="alert">
        {{ session('message') }}
    </div>
@endif

@if (session('messages'))
    <div class="alert alert-success animated fadeIn" role="alert">
        <pre>
        {{ print_r(session('messages'), true) }}
        </pre>
    </div>
@endif

@php
    /*
    @if ($errors->any())
        @foreach ($errors->all() as $key => $error)
            <div class="alert alert-danger animated fadeIn" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif
    */
@endphp

@if (session('success'))
    <div class="alert alert-success animated fadeIn text-center" role="alert">
        {{ session('success') }}
    </div>
@endif

@if (session('error'))
    <div class="alert alert-danger animated fadeIn text-center" role="alert">
        {{ session('error') }}
    </div>
@endif

@if ($errors->any())
    <div class="alert alert-danger animated fadeIn" role="alert">
        <ul class="list m-0">
        @foreach ($errors->all() as $key => $error)
            <li class="text-danger">{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif
