@if (app()->environment(['local', 'staging']))
<!-- Ribbon -->
<div class="corner-ribbon env-{{ config('app.env') }} top-left sticky shadow">{{ (trans()->has('app.env.' . config('app.env')) ? __('app.env.' . config('app.env')) : null) ?? (trans()->has('Development') ? __('Development') : null) ?? ucfirst(config('app.env')) }}</div>​
@endif