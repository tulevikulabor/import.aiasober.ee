@extends('layouts.app')

@section('title')
{{ __('Images') }}
@endsection

@section('content')

@component('components.card', ['title' => __('Images')])
    <div class="row">
        @foreach ($images as $image)
        <div class="col col-6 col-sm-4 col-md-3 col-lg-2">
            <div class="image-wrapper">
                <a href="{{ asset('imported_images/' . basename($image->path) . '/' . $image->filename) }}" class="image-link"><img src="{{ asset('imported_images/' . basename($image->path) . '/' . $image->filename) }}" class="image img-fluid" data-toggle="tooltip" data-placement="bottom" title="{{ $image->filename }}" /></a>
            </div>
        </div>
        @endforeach
    </div>
    {{ $images->links() }}
@endcomponent

<div class="image-modal modal fade" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Sulge</span></button>
                <img src="" class="image-preview" />
            </div>
        </div>
    </div>
</div>

@endsection

@push('styles')
<style>
    .image-wrapper {
        width: 100%;
        min-width: 100%;
        height: 130px;
        max-height: 130px;
        overflow: hidden;
        margin-bottom: 33px;
        
        border-radius: 4px;
        transition: box-shadow 0.5s ease, transform 0.15s;
        position: relative;
        z-index: 15;
    }
    
    .image-wrapper:hover {
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
        transform: scale(1.03);
        z-index: 16;
    }
    
    .image {
        width: 100%;
        height: 100%;
        
        object-fit: cover;
        object-position: 50% 50%;
    }
    
    .image-modal button {
        position: absolute;
        right: 30px;
        top: 20px;
        color: white;
        opacity: 1;
        font-size: 35px;
        text-shadow: 0, 0, 5px, 0 rgba(0, 0, 0, 0.5);
    }
    
    .image-modal .modal-content {
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
        border: none;
    }
    
    .image-preview {
        width: 100%;
        border-radius: 4px;
    }
</style>
@endpush

@push('scripts')
<script type="text/javascript">
    $(function() {
        $('a.image-link').click(function(e) {
            e.preventDefault();
            $('.image-preview').attr('src', $(this).find('img').attr('src'));
            $('#image-modal').modal('show');
        });
    });
</script>
@endpush