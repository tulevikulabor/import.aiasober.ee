<form id="upload-widget" method="post" action="{{ route($route) }}" class="dropzone">
    <div class="fallback">
        <input name="{{ $name ?? 'file' }}" type="file" />
    </div>
</form>

@push('styles')
    <!-- Dropzone -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/dropzone/dropzone.min.css') }}">
@endpush

@push('scripts')
    <!-- Dropzone -->
    <script src="{{ asset('assets/vendor/dropzone/dropzone.min.js') }}"></script>
    
    {{ $slot ?? '' }}
    
@endpush