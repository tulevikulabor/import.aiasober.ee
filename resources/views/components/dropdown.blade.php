<div class="btn-group">
    <button class="btn btn-main btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ $title }}
    </button>
    <div class="dropdown-menu dropdown-menu-right py-0">
        @foreach ($items as $item)
            @if(($item['display'] == true))
                <a class="dropdown-item{{ ($item['disabled'] == true) ? ' disabled' : null }}" href="{{ $item['url'] }}">{{ $item['name'] }}</a>
            @endif
        @endforeach
    </div>
</div>