<div id="dropzone-fullscreen" class="dropzone-fullscreen">
    <span>{{ __('Drag files here') }}</span>
</div>

<div class="text-center pb-3">
    <button id="dropzone-browse" class="btn dz-clickable btn-main">{{ __('Upload files') }}</button>
</div>

<form action="{{ $url }}" method="post" enctype="multipart/form-data" class="dropzone dz-clickable" id="file-upload">
    @csrf
    <div class="dz-default dz-message">
        <button class="dz-button" type="button">{{ __('Drag files here') }}</button>
    </div>
</form>

@push('styles')
<!-- Dropzone -->
<link rel="stylesheet" href="{{ asset('assets/vendor/dropzone/dropzone.min.css') }}">
@endpush

@push('scripts')
<!-- Dropzone -->
<script src="{{ asset('assets/vendor/dropzone/dropzone.min.js') }}"></script>
<script type="text/javascript">
    Dropzone.options.fileUpload = {
        paramName: "{{ $paramName ?? 'file' }}",
        maxFilesize: {{ $maxFilesize ?? 5 }}, // MB
        maxThumbnailFilesize: {{ $maxFilesize ?? 10 }}, // MB
        parallelUploads: 1,
        clickable: "#dropzone-browse", // Define the element that should be used as click trigger to select files.
        
        @if($maxFiles)
        maxFiles: {{ $maxFiles ?? null }}, // default: 256
        @endif
        
        @if($acceptedFiles)
        acceptedFiles: "{{ is_array($acceptedFiles) ? implode(',', $acceptedFiles) : $acceptedFiles }}",
        @endif
        
        error: function(file, response) {
            if ($.type(response) === "string") {
                var message = response; //dropzone sends it's own error messages in string
            } else {
                var message = response.errors.{{ $paramName ?? 'file' }}[0];
            }
            file.previewElement.classList.add("dz-error");
            _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
        
            _results = [];
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i];
                _results.push(node.textContent = message);
            }
            return _results;
        },
    };
    
    /*var dropZone = document.getElementById('file-upload');

    function showDropZone() {
        dropZone.style.display = "block";
    }

    function hideDropZone() {
        dropZone.style.display = "none";
    }

    function allowDrag(e) {
        if (true) { // Test that the item being dragged is a valid one
            e.dataTransfer.dropEffect = 'copy';
            e.preventDefault();
        }
    }

    function handleDrop(e) {
        e.preventDefault();
        hideDropZone();

        alert('Drop!');
    }

    // 1
    window.addEventListener('dragenter', function(e) {
        showDropZone();
    });

    // 2
    dropZone.addEventListener('dragenter', allowDrag);
    dropZone.addEventListener('dragover', allowDrag);

    // 3
    dropZone.addEventListener('dragleave', function(e) {
        hideDropZone();
    });
    
    dropZone.addEventListener('maxfilesexceeded', function(file) {
        alert('Liiga palju faile, maksimaalselt lubatud ');
        //dropZone.removeAllFiles();
        //dropZone.addFile(file);
    });

    // 4
    dropZone.addEventListener('drop', handleDrop); */
</script>

@endpush