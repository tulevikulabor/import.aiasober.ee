@if($total_pages > 1)
<nav aria-label="...">
    <ul class="pagination pt-3">
        
        @if($current_page != 1)
            <li class="page-item">
                <a class="page-link" href="{{ url()->current() }}?page=1">{!! __('First') !!}</a>
            </li>
        @else
            <li class="page-item disabled">
                <span class="page-link">{!! __('First') !!}</span>
            </li>
        @endif
        
        @if($previous_page)
            <li class="page-item">
                <a class="page-link" href="{{ url()->current() }}?page={{ $previous_page }}">{{ __('Previous') }}</a>
            </li>
        @else
            <li class="page-item disabled">
                <span class="page-link">{{ __('Previous') }}</span>
            </li>
        @endif
        
        @if($total_pages > 20)
            @if(request()->input('page') == $current_page || (!request()->input('page') && $current_page == 1))
                <li class="page-item active" aria-current="page"><span class="page-link">{{ $current_page }}</span></li>
            @endif
            <li class="page-item">
                <div class="dropdown">
                    <span class="page-link" type="button" id="dropdownPages" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('...') }}</span>
                    <div class="dropdown-menu p-0" aria-labelledby="dropdownPages">
                    @for ($page = 1; $page <= $total_pages; $page ++)
                        <a class="dropdown-item @php if(request()->input('page') == $page || (!request()->input('page') && $page == 1)) {echo 'active'; } @endphp" href="{{ url()->current() }}?page={{ $page }}">{{ $page }}</a>
                    @endfor
                    </div>
                </div>
            </li>
        @else
            @for ($page = 1; $page <= $total_pages; $page ++)
                @if(request()->input('page') == $page || (!request()->input('page') && $page == 1))
                    <li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                @else
                    <li class="page-item"><a class="page-link" href="{{ url()->current() }}?page={{ $page }}">{{ $page }}</a></li>
                @endif
            @endfor
        @endif
        
        @if($next_page)
            <li class="page-item">
                <a class="page-link" href="{{ url()->current() }}?page={{ $next_page }}">{{ __('Next') }}</a>
            </li>
        @else
            <li class="page-item disabled">
                <span class="page-link">{{ __('Next') }}</span>
            </li>
        @endif
        
        @if($current_page != $total_pages)
            <li class="page-item">
                <a class="page-link" href="{{ url()->current() }}?page={{ $total_pages }}">{!! __('Last') !!}</a>
            </li>
        @else
            <li class="page-item disabled">
                <span class="page-link">{!! __('Last') !!}</span>
            </li>
        @endif
        
    </ul>
</nav>
@endif