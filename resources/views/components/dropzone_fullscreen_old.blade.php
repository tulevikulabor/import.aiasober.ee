<form id="dropzone" class="dropzone-fullscreen align-items-center" action="{{ $url }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="fallback">
        <input name="{{ $paramName ?? 'file' }}" type="file" multiple accept="{{ is_array($acceptedFiles) ? implode(',', $acceptedFiles) : $acceptedFiles }}" />
    </div>
    <span class="dz-message">{{ __('Drag files here') }}</span>
</form>

<div class="text-center">
    <button id="dropzone-browse" class="btn btn-main">{{ __('Upload files') }}</button>
    <button id="dropzone-browse2" class="btn btn-main">{{ __('Upload') }}</button>
</div>

<div id="dropzone-previews" class="dropzone-previews w-100"></div>

@push('styles')
<!-- Dropzone -->
<link rel="stylesheet" href="{{ asset('assets/vendor/dropzone/dropzone.min.css') }}">
@endpush

@push('scripts')
<!-- Dropzone -->
<script src="{{ asset('assets/vendor/dropzone/dropzone.min.js') }}"></script>

{{ $slot ?? '' }}

<script>
    var myDropzone = new Dropzone('#dropzone', { // Make the whole body a dropzone: document.body
        autoProcessQueue: true,
        paramName: "{{ $paramName ?? 'file' }}", // The name that will be used to transfer the file
        //url: "{{ $url }}", // Set the url
        //previewsContainer: "#dropzone-previews", // Define the container to display the previews
        clickable: "#dropzone-browse", // Define the element that should be used as click trigger to select files.
        maxFilesize: {{ $maxFilesize ?? 5 }}, // MB
        addRemoveLinks: false,
        parallelUploads: 2,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), // {{ csrf_token() }}
        },
        
        @if($maxFiles)
        maxFiles: {{ $maxFiles ?? null }}, // default: 256
        @endif
        
        @if($acceptedFiles)
        acceptedFiles: "{{ is_array($acceptedFiles) ? implode(',', $acceptedFiles) : $acceptedFiles }}",
        @endif
    });

    var dropZone = document.getElementById('dropzone');

    function showDropZone() {
        dropZone.style.display = "block";
    }

    function hideDropZone() {
        dropZone.style.display = "none";
    }

    function allowDrag(e) {
        if (true) { // Test that the item being dragged is a valid one
            e.dataTransfer.dropEffect = 'copy';
            e.preventDefault();
        }
    }

    function handleDrop(e) {
        e.preventDefault();
        hideDropZone();

        alert('Drop!');
        
        // myDropzone.processQueue();
    }

    // 1
    window.addEventListener('dragenter', function(e) {
        showDropZone();
    });

    // 2
    dropZone.addEventListener('dragenter', allowDrag);
    dropZone.addEventListener('dragover', allowDrag);

    // 3
    dropZone.addEventListener('dragleave', function(e) {
        console.log('dragleave');
        hideDropZone();
    });
    
    dropZone.addEventListener('maxfilesexceeded', function(file) {
        alert('Liiga palju faile, maksimaalselt lubatud ');
        //dropZone.removeAllFiles();
        //dropZone.addFile(file);
    });

    // 4
    dropZone.addEventListener('drop', handleDrop);
</script>
@endpush