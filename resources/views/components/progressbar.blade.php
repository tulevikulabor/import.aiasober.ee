@php
if ($valuemax > 0) {
    $progress = floor(($value / $valuemax) * 100);
} else {
    $progress = 0;
}
@endphp
<div id="{{ $id }}" class="progress" style="height: 26px;">
    <div class="progress-bar progress-bar-striped progress-bar-animated bg-{{ $bg ?? 'success' }}" role="progressbar" aria-valuenow="{{ $value ?? 0 }}" aria-valuemin="{{ $valuemin ?? 0 }}" aria-valuemax="{{ $valuemax ?? 0 }}" style="width: {{ $progress }}%">
        @if(boolval($isPercentage))
            {{ $progress }}%
        @else
            <span class="text-white"><span class="value text-white">{{ $value }}</span><span class="separator px-1 text-white">/</span><span class="valuemax text-white">{{ $valuemax }}</span></span>
        @endif
    </div>
    @if(strlen($slot) > 1)
        {{ $slot }}
    @else
        @if($url)
            @push('scripts')
                <script type="text/javascript">
                    $(function () {
                        
                        function refreshProgressBar{{ Str::ucfirst(Str::camel($id)) }} () {
                            
                            var progress_previous = null;
                                                        
                            $.ajax({
                                type: 'GET',
                                url: '{{ $url }}',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function (data) {
                                    console.log(data);
                                    if(data) {
                                                
                                        var progressbar = $('#{{ $id }}');
                                        var progress = Math.floor((data.value / data.max) * 100);
                                        progress_previous = Number($('.progress-bar', progressbar).attr('aria-valuenow'));
                                        
                                        console.log('Progress: ' + progress + '%');
                                        console.log('Progress (previous): ' + progress_previous + '%');
                                        
                                        $('.progress-bar', progressbar).attr('style', 'width: ' + data.progress_exact + '%');
                                        
                                        if(progress < 100) {
                                            $('.progress-bar', progressbar).addClass('progress-bar-striped');
                                            @isset($spinnerId)
                                                $('#{{ $spinnerId }}').show('slow');
                                            @endisset
                                            
                                        } else {
                                            $('.progress-bar', progressbar).removeClass('progress-bar-striped');
                                            @isset($spinnerId)
                                                $('#{{ $spinnerId }}').hide('slow');
                                            @endisset
                                            
                                            if(progress_previous !== progress && (progress_previous > 0 || data.max == 1) && progress == 100) {
                                                console.log('Progress: ' + progress + '%, Progress (previous): ' + progress_previous + '% AND refresh()');
                                            }
                                        }

                                        @if(boolval($isPercentage))
                                            $('.progress-bar', progressbar).text(progress + '%');
                                            $('.progress-bar', progressbar).attr('aria-valuemin', 0);
                                            $('.progress-bar', progressbar).attr('aria-valuemax', 100);
                                            $('.progress-bar', progressbar).attr('aria-valuenow', progress);
                                        @else
                                            $('.progress-bar .value', progressbar).text(data.value);
                                            $('.progress-bar .valuemax', progressbar).text(data.max);
                                        @endif
                                        
                                    } else {
                                        // no active session
                                    }
                                }
                            });
                        }
                        
                        refreshProgressBar{{ Str::ucfirst(Str::camel($id)) }} ();
                        
                        setInterval(function () {
                            refreshProgressBar{{ Str::ucfirst(Str::camel($id)) }} ();
                        }, {{ $interval * 1000 }});
                    });
                </script>
            @endpush
        @endif
    @endif
</div>
