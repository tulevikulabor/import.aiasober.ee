@if(isset($wide) && $wide == 1)
<div class="container-fluid pb-4">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $title }}
                    @isset($badges)
                        @foreach ($badges as $key => $badge)
                            <span id="{{ $badge['id'] ?? 'badge-' . ($key + 1) }}" class="badge badge-pill badge-{{ $badge['color'] ?? 'primary' }} ml-3 px-3 py-2">{{ $badge['name'] }}</span>
                        @endforeach
                    @endisset
                </div>

                <div class="card-body overflow-auto">
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="container pb-4">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex align-items-center justify-content-between">{{ $title }}
                    @isset($badges)
                        @foreach ($badges as $key => $badge)
                            <span id="{{ $badge['id'] ?? 'badge-' . ($key + 1) }}" class="badge badge-pill badge-{{ $badge['color'] ?? 'primary' }} ml-3 px-3 py-2">{{ $badge['name'] }}</span>
                        @endforeach
                    @endisset
                    @isset($spinner)
                        @if($spinner['active'])
                            <div id="{{ $spinner['id'] }}" class="spinner-border text-success spinner-border-sm mr-1" role="status"></div>
                        @endif
                    @endisset
                </div>

                <div class="card-body">
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>
</div>
@endif